import math
import torch


def sinc(band, t_right):
    """Build a lowpass sinc signal
        y = sin( 2pift )
            ------------
                2pift
        
        Paramters
        ----------
        band : float
            band of the signal
        t_right : iterable
            right side of the time axis
    """

    y_right = torch.sin(2 * math.pi * band * t_right) / (2 * math.pi * band * t_right)
    y_left = torch.flip(y_right, (0,))

    one = torch.ones(1)

    if torch.cuda.is_available() and t_right.is_cuda:
        one = one.cuda(non_blocking=True)

    y = torch.cat([y_left, one, y_right])

    return y


def sinc2D(band, xRight, yRight):
    """Build a low pass sinc signal on a two dimensional domain

        
    """

    sincX = sinc(band, xRight)
    sincY = sinc(band, yRight)

    return torch.mm(sincX.view(-1, 1), sincY.view(1, -1))


def secondOrderResponse(w, xi, t):

    return (
        (w / torch.sqrt(1 - (xi ** 2)))
        * torch.exp(-1 * xi * w * t)
        * torch.sin(xi * torch.sqrt(1 - xi ** 2))
    )


def filterRLC(A, alpha, wd, phi, t):

    return A * (torch.exp(-1 * alpha * t) * torch.sin(wd * t + phi))


def modCosine(band, t):

    y = torch.cos(2 * math.pi * band * t) * torch.exp(-1 * 2 * math.pi * t * t)

    return y


def Haar(t):
    y = torch.zeros(t.shape, dtype=torch.float32)
    for idx, tSample in enumerate(t):
        if tSample <= 0.5 and tSample >= 0:
            y[idx] = 1.0
        elif tSample <= 1 and tSample > 0.5:
            y[idx] = -1.0

    return y
