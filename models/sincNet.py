class SincNet(nn.Module):

    def __init__(self, net_param):

        super(SincNet, self).__init__()

        self.act = net_param["act"]
        if net_param.get("viewer", None):
            self.viewer = net_param["viewer"]

        self.dropout = nn.Dropout(p=net_param["dropout"])

        self.fs = net_param["fs"]  # frequency of sampling
        self.N_sinc = net_param["sinc_filters"]  # number of sinc filters
        self.sinc_dim = net_param["sinc_size"]  # sinc filter size
        self.T = net_param["T"]  # Time points per sample
        self.N = net_param["N"]  # Number of classes
        self.C = net_param["C"]  # Number of channels
        self.pool_size = net_param["pool_size"]  # Size of the pooling filter

        self.N_conv = net_param[
            "conv_depth"
        ]  # Number of convolutional filters per layer
        self.conv_size = net_param["conv_size"]  # Kernel size of convolutional filters
        self.linear_units = net_param[
            "linear_units"
        ]  # Number of units inside the Linear layers

        # BLOCK 1 --> Sinc convolution
        self.sinc_conv = Sinc_conv(
            self.N_sinc, self.C, self.sinc_dim, self.fs, mode="1D"
        )

        self.max_pool1 = nn.MaxPool1d(self.pool_size)
        self.layer_norm1 = nn.LayerNorm(([self.N_sinc, self.T / self.pool_size]))
        
        # BLOCK 2 --> 2 conv layers
        self.conv1 = nn.Conv1d(
            self.N_sinc,
            self.N_conv,
            kernel_size=self.conv_size,
            padding=int(self.conv_size / 2),
        )

        self.layer_norm2 = nn.LayerNorm([self.N_conv, int(self.T / self.pool_size)])

       self.conv2 = nn.Conv1d(
            self.N_conv,
            self.N_conv,
            kernel_size=self.conv_size,
            padding=int(self.conv_size / 2),
        )

        self.max_pool2 = nn.MaxPool1d(self.pool_size)
        self.layer_norm3 = nn.LayerNorm(
            [self.N_conv, int(self.T / self.pool_size ** 2)]
        )

        # CLASSIFICATION
        self.fc1 = nn.Linear(
            in_features=int(self.N_conv * int(self.T / (self.pool_size ** 2))),
            out_features=self.linear_units,
        )
        self.bn4 = nn.BatchNorm1d(self.linear_units)

        self.fc2 = nn.Linear(
            in_features=self.linear_units, out_features=self.linear_units
        )
        self.bn5 = nn.BatchNorm1d(self.linear_units)

        self.fc3 = nn.Linear(in_features=self.linear_units, out_features=self.N)

        # Weights initialization
        nn.init.xavier_normal_(self.conv1.weight)
        nn.init.xavier_normal_(self.conv2.weight)
        nn.init.xavier_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)
        nn.init.xavier_normal_(self.fc3.weight)

    def forward(self, x):

        # BLOCK 1
        x = self.sinc_conv(x)
        # x = self.bn1( x )
        x = self.max_pool1(x)
        x = self.act(x)
        x = self.layer_norm1(x)
        x = self.dropout(x)

        x = self.conv1(x)
        # x = self.bn2(x)
        x = self.act(x)
        x = self.layer_norm2(x)
        x = self.conv2(x)
        # x = self.bn3( x )
        x = self.max_pool2(x)
        x = self.act(x)

        x = self.layer_norm3(x)
        x = self.dropout(x)

        # FLATTEN

        x = x.view(-1, int(self.N_conv * int(self.T / (self.pool_size ** 2))))

        x = self.fc1(x)
        x = self.act(x)
        x = self.bn4(x)
        x = self.dropout(x)

        x = self.fc2(x)
        x = self.act(x)
        x = self.bn5(x)
        x = self.dropout(x)

        x = self.fc3(x)

        return x
