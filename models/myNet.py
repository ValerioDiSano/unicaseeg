from sincLayer import Sinc_conv
import torch.nn as nn

class SincEEG(nn.Module):
    def __init__(self, net_param):

        super(SincEEG, self).__init__()

        self.act = net_param["act"]
        self.dropout = nn.Dropout(p=net_param["dropout"])
        self.clipper = net_param["clipper"]

        if net_param.get("viewer", None):
            self.viewer = net_param["viewer"]

        self.fs = net_param["fs"]  # frequency of sampling
        self.F1 = net_param["sinc_filters"]  # number of sinc filters
        self.sinc_dim = net_param["sinc_size"]  # sinc filter size
        self.C = net_param["C"]  # number of channels
        self.D = net_param["depth"]  # depth of depth-wise convolution layer
        self.T = net_param["T"]  # Time points per sample
        self.N = net_param["N"]  # Number of classes
        self.poolSize = net_param["pool_size"]

        self.kernelSize = int(self.fs / 8)

        # Verify that the filter length is odd
        if self.kernelSize % 2 == 0:
            self.kernelSize = self.kernelSize + 1
        
        self.F2 = self.D * (self.F1)

        self.convLayer = Sinc_conv(
            channelsOut=self.F1, channelsIn=1, funcPoints=self.sinc_dim, fs=self.fs
        )
        
        self.pool0 = nn.AvgPool2d((1, self.poolSize))

        self.ln1 = nn.LayerNorm([self.F1, self.C, self.T / self.poolSize])

        self.depthWise1 = nn.Conv2d(
            in_channels=self.F1,
            out_channels=self.F2,
            kernel_size=(self.C, 1),
            groups=self.F1,
        )

        self.ln2 = nn.LayerNorm([self.F2, self.C, self.T / (self.poolSize**2)])

        self.pool1 = nn.AvgPool2d((1, self.poolSize))

        self.depthWise2 = nn.Conv2d(
            in_channels=self.F2,
            out_channels=self.F2,
            kernel_size=(1, self.kernelSize),
            groups=self.F2,
            padding=(0, self.kernelSize / 2),
        )
        self.lnDW = nn.LayerNorm([self.F2, 1, self.T / (self.poolSize**2)])
        self.pointConv = nn.Conv2d(
            in_channels=self.F2, out_channels=self.F2, kernel_size=1
        )

        self.ln3 = nn.LayerNorm([self.F2, 1, self.T / (self.poolSize**3)])

        self.pool2 = nn.AvgPool2d((1, self.poolSize))

        self.linUnits = self.F2 * (self.T / (self.poolSize ** 3))
        self.classifier = nn.Linear(in_features=self.linUnits, out_features=self.N)

        nn.init.xavier_normal_(self.depthWise1.weight)
        nn.init.xavier_normal_(self.depthWise2.weight)
        nn.init.xavier_normal_(self.pointConv.weight)
        nn.init.xavier_normal_(self.classifier.weight)

    def forward(self, x):

        x = self.convLayer(x)
        x = self.pool0(x)
        x = self.ln1(x)
        x = self.act(x)
        x = self.dropout(x)

        x = self.depthWise1(x)
        x = self.pool1(x)
        x = self.ln2(x)
        x = self.act(x)
        x = self.dropout(x)

        x = self.depthWise2(x)
        x = self.lnDW(x)
        x = self.act(x)
        x = self.dropout(x)

        x = self.pointConv(x)
        x = self.pool2(x)
        x = self.ln3(x)
        x = self.act(x)
        x = self.dropout(x)

        # Classification
        x = x.view(-1, self.linUnits)
        x = self.classifier(x)

        return x
