import torch.nn as nn
import torch
import math
import numpy as np

from signals.signals import sinc
from signals.signals import sinc2D

from utils.dataOp import freq2mel
from utils.dataOp import mel2freq

import torch.nn.functional as F


import matplotlib
matplotlib.use("Agg") # Necessary in order to run the code on a machine without a graphical environment
import matplotlib.pyplot as plt

class SincViewer(object):
    def __init__(self, path=""):
        self.path = path

    def __call__(self, module):

        if not os.path.exists(self.path):
            os.makedirs(self.path)

        for (_, module) in module.named_modules():

            if type(module).__name__ == "Sinc_conv":

            
                f1 = module.f1.data
                if f1.is_cuda:
                    f1 = f1.cpu()
                band = module.band.data
                if band.is_cuda:
                    band = band.cpu()

                t_right = (
                    torch.linspace(
                        1,
                        (module.funcPoints - 1) / 2,
                        steps=int((module.funcPoints - 1) / 2),
                    )
                    / module.fs
                )
                t_left = torch.flip(t_right, (0,))
                t_left *= -1
                zero = torch.zeros(1)

                t = torch.cat([t_left, zero, t_right])
                # High frequency = low frequency + signal band
                
                f2 = f1 + band
                
                for i in range(module.channelsOut):

                    sinc1 = 2 * f1[i] * sinc(f1[i], t_right)
                    sinc2 = 2 * f2[i] * sinc(f2[i], t_right)

                    bandpass_filter = sinc2 - sinc1
                    
                    bandpass_filter = bandpass_filter / torch.max(bandpass_filter)

                    plt.plot(t.tolist(), bandpass_filter.tolist())
                    plt.savefig(
                        os.path.join(
                            self.path,
                            str(i)
                            + "_"
                            + str(f1[i].item())
                            + "_"
                            + str(band[i].item())
                            + ".png",
                        )
                    )
                    plt.clf()




class Sinc_conv(nn.Module):

    """ Sinc_conv : Convolutional layer based on filters with a sinc shape
        Args:
            N_filt_out ( int ): number of sinc filters allocated / number of output channels
            channels_in ( int ): number of input channels of the layer
            fitler_dim( int ): size of each sinc filter
            fs ( int ): frequency sampling of the input signal [Hz]
    """

    def __init__(
        self,
        channelsOut,
        channelsIn,
        funcPoints,
        fs,
        init="random",
        mode="2D"
    ):
        super(Sinc_conv, self).__init__()

        self.fs = fs  # sampling frequency of the input signal
        self.channelsOut = channelsOut  # number of stacked filters
        self.funcPoints = funcPoints  # number of points per Sinc function
        self.channelsIn = channelsIn  # channels of the input tensor
        self.init = init  # initialization mode
        self.mode = mode  # 2D convolution / 1D convolution

        # layer parameters(f1,band) initialization
        # f1 -> low frequency
        # f2 -> high frequency
        # band -> (f2 - f1)

        # Weights initialization
        if self.init == "random":
            self.f1 = [
                np.abs(np.random.normal(self.fs / 4.0, self.fs / 4.0, 1))
                for i in range(self.channelsOut)
            ]
            self.f2 = [
                self.f1[i] + np.abs(np.random.normal(0.0, 15.0, 1) - self.f1[i])
                for i in range(self.channelsOut)
            ]
        elif self.init == "mel_scale":
            low_mel = 40
            high_mel = freq2mel(self.fs / 2.0)

            mel_pts = np.linspace(low_mel, high_mel, self.channelsOut)
            f_pts = np.array([mel2freq(f) for f in mel_pts])

            self.f1 = np.roll(f_pts, 1)
            self.f2 = np.roll(f_pts, -1)

            self.f1 = np.divide(self.f1, self.fs)
            self.f2 = np.divide(self.f2, self.fs)

            self.f1[0] = 5  # [hz]
            self.f2[-1] = self.fs / 2.0

        self.band = torch.FloatTensor(np.subtract(self.f2, self.f1))
        self.f2 = torch.FloatTensor(self.f2)
        self.f1 = torch.FloatTensor(self.f1)
    
    
        # parameters to GPU
        if torch.cuda.is_available():
                
            self.f1 = self.f1.cuda(non_blocking=True)
            self.f2 = self.f2.cuda(non_blocking=True)
            self.band = self.band.cuda(non_blocking=True)
            

        # Updatable parameters of the layer
        self.f1 = nn.Parameter(self.f1)
        self.band = nn.Parameter(self.band)

    def forward(self, x):
        
        #Filter allocation
        filters = torch.zeros(self.channelsOut, self.funcPoints)

        if torch.cuda.is_available():
            filters = filters.cuda(non_blocking=True)

        # Time vector
        t_right = (
            torch.linspace(
                1, (self.funcPoints - 1) / 2, steps=int((self.funcPoints - 1) / 2)
            )
            / self.fs
        )

        if torch.cuda.is_available():
            t_right = t_right.cuda(non_blocking=True)

        # High frequency = low frequency + signal band
        self.f2 = self.f1 + self.band

        n = torch.linspace(
            0, self.funcPoints, steps=self.funcPoints
        )
        # Hamming windowing
        window = 0.54 - 0.46 * torch.cos(2 * math.pi * n / self.funcPoints)

        if torch.cuda.is_available():
            window = window.cuda(non_blocking=True)

        for i in range(self.channelsOut):


            sinc1 = 2 * self.f1[i] * sinc(self.f1[i], t_right)
            sinc2 = 2 * self.f2[i] * sinc(self.f2[i], t_right)
            bandpass_filter = sinc2 - sinc1

            if torch.cuda.is_available():
                bandpass_filter = bandpass_filter.cuda(non_blocking=True)

            filters[i, :] = torch.mul(
                (bandpass_filter / torch.max(bandpass_filter)), window
            )
        
        ## TODO: Accept input with number of channels greater than 1
        """
        if ( self.channelsIn > 1 ):
            #print "Multiple In channels"
            if ( self.mode == "2D"):
                if self.transpose:
                    out = F.conv2d(x, filters.view(self.channelsOut,1,self.funcPoints,1).repeat(1,self.channelsIn,1,1),padding= (int(self.funcPoints/2),0 ) )
                else:
                    out = F.conv2d(x, filters.view(self.channelsOut,self.channelsIn, 1 ,self.funcPoints),padding= (0, int(self.funcPoints/2) ) )
            elif( self.mode == "1D"):
                out = F.conv1d(x, filters.view(self.channelsOut,1,self.funcPoints).repeat( 1, self.channelsIn, 1 ), padding= int(self.funcPoints/2))
                #out = F.conv1d(x, filters.view(self.channelsOut,1,self.funcPoints), padding= int(self.funcPoints/2), groups = self.channelsIn)
            
            #out = F.conv1d( x, filters.view( self.N_filt_out, 1 , self.filter_dim ).repeat( 1, self.channels_in, 1 ), padding= int(self.filter_dim/2) )
            #print x.shape
            #out = F.conv2d( x, filters.view(self.N_filt_out,1, 1 ,self.filter_dim).repeat(  1, self.channels_in ,1, 1 ), padding= (0, int(self.filter_dim/2) ) )
            
        else:
        """
        if self.mode == "2D":

            out = F.conv2d(
                x,
                filters.view(self.channelsOut, self.channelsIn, 1, self.funcPoints),
                padding=(0, int(self.funcPoints / 2)),
            )
        elif self.mode == "1D":
            out = F.conv1d(
                x,
                filters.view(self.channelsOut, 1, self.funcPoints),
                padding=int(self.funcPoints / 2),
            )

        torch.cuda.empty_cache()
        return out


class SincConv2D(Sinc_conv):
    def __init__(self, channelsOut, channelsIn, timePts, channPts, fs, stride=1):

        super(SincConv2D, self).__init__(channelsOut, channelsIn, timePts, fs)
        self.channPts = channPts
        self.stride = stride

    def forward(self, x):

        # High frequency = low frequency + signal band
        self.f2 = self.f1 + self.band

        filters = torch.zeros(self.channelsOut, self.funcPoints, self.channPts)
        # Time vector
        tRight = (
            torch.linspace(
                1, (self.funcPoints - 1) / 2, steps=int((self.funcPoints - 1) / 2)
            )
            / self.fs
        )
        cVec = (
            torch.linspace(
                1, (self.channPts - 1) / 2, steps=int((self.channPts - 1) / 2)
            )
            / self.fs
        )

        n1 = torch.linspace(
            0, self.funcPoints, steps=self.funcPoints
        )  # / self.fs ############
        n2 = torch.linspace(0, self.channPts, steps=self.channPts)
        # Hamming windowing
        Hamming = lambda n, points: 0.54 - 0.46 * torch.cos(2 * math.pi * n / points)
        window1 = Hamming(n1, self.funcPoints)
        window2 = Hamming(n2, self.channPts)
        window = torch.mm(window1.view(-1, 1), window2.view(1, -1))

        if torch.cuda.is_available():
            filters = filters.cuda(non_blocking=True)
            tRight = tRight.cuda(non_blocking=True)
            cVec = cVec.cuda(non_blocking=True)
            window = window.cuda(non_blocking=True)

        for i in range(self.channelsOut):

            sinc1 = (
                4 * self.f1[i] * self.f1[i] * sinc2D(self.f1[i], tRight, cVec)
            )
            sinc2 = (
                4 * self.f2[i] * self.f2[i] * sinc2D(self.f2[i], tRight, cVec)
            )
            bandpass_filter = sinc2 - sinc1

            if torch.cuda.is_available():
                bandpass_filter = bandpass_filter.cuda(non_blocking=True)

            filters[i, :, :] = (bandpass_filter / torch.max(bandpass_filter)) * window

        out = F.conv2d(
            x,
            filters.view(self.channelsOut, 1, self.channPts, self.funcPoints),
            padding=(int(self.channPts / 2), int(self.funcPoints / 2)),
            stride=self.stride,
        )

        return out

