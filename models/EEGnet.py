import torch.nn as nn
import math

class EEGnet(nn.Module):
    """ Implementation of the EEGnet for BCI signals classification

        Args
        -----------
        F1 : int
            number of features after the first convolutional layer
        C : int
            number of channels of the 
            D (int):
            T (int):
            N (int):
            activation (object):
            dropout (float):
            clipper (object):
        Attributes:
            Args
            F2 (int):
            b1_kernel (int):
            soft_max ():

    """

    def __init__(self, F1, C, D, T, N, activation, dropout, clipper=None):

        self.C = C
        self.F1 = F1
        self.b1_kernel = 64
        self.D = D
        self.F2 = D * F1

        self.T = T  # Time points per sample
        self.N = N  # Number of classes

        super(EEGnet, self).__init__()

        self.act = activation
        self.clipper = clipper
        self.dropout = nn.Dropout(p=dropout)

        # BLOCK 1
        self.conv1 = nn.Conv2d(
            in_channels=1,
            out_channels=self.F1,
            kernel_size=(1, self.b1_kernel),
            padding=(0, int(self.b1_kernel / 2)),
        )
        self.bn1 = nn.BatchNorm2d(self.F1)
        self.depthWise1 = nn.Conv2d(
            in_channels=self.F1,
            out_channels=self.F2,
            kernel_size=(self.C, 1),
            padding=0,
            groups=self.F1,
        )
        self.bn2 = nn.BatchNorm2d(self.F2)
        self.avgPool1 = nn.AvgPool2d((1, 4), stride=(1, 4))

        # BLOCK 2
        # Seprable convolution
        # self.sep = nn.Conv2d( in_channels = 1, out_channels = self.D*self.F1, kernel_size = (1,16),padding= (0, int( 8 ) ) )
        self.depthWiseConv2 = nn.Conv2d(
            in_channels=self.F2,
            out_channels=self.F2,
            kernel_size=(1, 16),
            groups=self.D * self.F1,
            padding=(0, int(8)),
        )
        self.pointwise = nn.Conv2d(
            in_channels=self.D * self.F1, out_channels=self.F2, kernel_size=1
        )
        # self.pointwise = nn.Conv2d( in_channels = self.D*self.F1, out_channels = 1, kernel_size = 1 )

        self.bn3 = nn.BatchNorm2d(self.F2)
        self.avgPool2 = nn.AvgPool2d((1, 8), stride=(1, 8))

        # CLASSIFICATION
        self.classifier = nn.Linear(
            in_features=int(self.F2 * math.floor(self.T / 32)), out_features=self.N
        )

        # Weights initialization
        nn.init.xavier_normal_(self.conv1.weight)
        nn.init.xavier_normal_(self.depthWise1.weight)
        nn.init.xavier_normal_(self.depthWiseConv2.weight)
        nn.init.xavier_normal_(self.pointwise.weight)
        nn.init.xavier_normal_(self.classifier.weight)

    def forward(self, x):

        x = self.conv1(x)
        x = self.bn1(x)
        x = self.depthWise1(x)
        x = self.bn2(x)
        x = self.act(x)
        x = self.avgPool1(x)
        x = self.dropout(x)
        x = self.depthWiseConv2(x)
        x = self.pointwise(x)

        x = self.bn3(x)
        x = self.act(x)
        x = self.avgPool2(x)
        x = self.dropout(x)

        # FLATTEN
        x = x.view(-1, int(self.F2 * math.floor(self.T / 32)))

        x = self.classifier(x)

        return x