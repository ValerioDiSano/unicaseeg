# -*- coding: utf-8 -*-

import argparse
from BCI_data.competition_III_4a import competition_III_4a
import torch
from models.myNet import SincEEG


def main():

    parser = argparse.ArgumentParser(prog="Test a trained network.\n")
    parser.add_argument(
        "--path", "-p", action="store", dest="path", help="Path to the trained network"
    )
    parser.add_argument(
        "--patience", action="store", dest="patience", help="Patience to test"
    )

    args = parser.parse_args()

    numWorkers = 16
    torch.cuda.set_device(1)

    data = competition_III_4a(
        sampleTransform=BCI_data.Convert(),
        labelTransform=BCI_data.Label_decrement(),
        preprocessing="FIR:normpersample:resample",
        newRate=128,
        superSampling=False,
    )

    # Only test data
    data.pop("train")
    data.pop("valid")

    bestModel = torch.load(str(args.path))
    print bestModel["epoch"]
    print bestModel["accuracy"]
    # bestModel['net'].dump_patches= True
    net = bestModel["net"]
    net = net.cuda()
    net.eval()
    # net.dump_patches = True
    dataloaderTest = torch.utils.data.DataLoader(
        data["test"],
        batch_size=len(data["test"]) / 4,
        num_workers=numWorkers,
        pin_memory=True,
    )
    predictions, scores = models.test(net, dataloaderTest)
    testAccuracy = (
        100
        * predictions.eq(torch.LongTensor((data["test"].get_labels()))).sum().float()
        / len(data["test"])
    )

    print "Overall accuracy: %f" % (testAccuracy,)

    for pat in ["aa", "al", "ay", "aw", "av"]:

        subset = torch.utils.data.Subset(
            data["test"],
            range(data["test"].sets[pat][0], data["test"].sets[pat][1] + 1),
        )
        dataloaderTest = torch.utils.data.DataLoader(
            subset,
            batch_size=len(data["test"]) / 4,
            num_workers=numWorkers,
            pin_memory=True,
        )
        print len(subset)
        predictions, scores = models.test(net, dataloaderTest)
        # predictions = torch.LongTensor( predictions )

        print (len(predictions))

        testAccuracy = (
            100
            * predictions.eq(
                torch.LongTensor(
                    (
                        data["test"].get_labels()[
                            data["test"].sets[pat][0] : data["test"].sets[pat][1] + 1
                        ]
                    )
                )
            )
            .sum()
            .float()
            / len(subset)
        )
        print "Patience %s: accuracy: %f" % (pat, testAccuracy)


if __name__ == "__main__":
    main()
