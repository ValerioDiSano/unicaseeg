import torch

def train(net, dataloader, scheduler, criterion, optimizer):

    net.train()  # Switch to train mode

    if scheduler:
        scheduler.step()  # Update Learning Rate

    # Variables init
    loss_sum = 0.0
    correct = 0

    # Iterate over the data
    for batch in dataloader:

        inputs, targets = batch

        if torch.cuda.is_available():
            inputs, targets = (
                inputs.cuda(non_blocking=True),
                targets.cuda(non_blocking=True),
            )

        optimizer.zero_grad()
        torch.cuda.empty_cache()    # it avoids high memory allocation

        outputs = net(inputs)       

        targets = targets.long()

        loss = criterion(outputs, targets)

        torch.cuda.empty_cache()

        loss.backward(retain_graph=False)

        optimizer.step()

        if hasattr(net, "clipper") and net.clipper:
            net.apply(net.clipper)

        loss_sum += loss.item()

        outputs_max = torch.argmax(outputs, dim=1)

        correct += outputs_max.eq(targets).sum().float()

    return loss_sum / len(dataloader), 100.0 * correct / len(dataloader.dataset)


def test(net, dataloader):

    net.eval()  # Switch to test mode

    predictions = torch.zeros(len(dataloader.dataset), dtype=torch.int64)

    scores = torch.zeros(len(dataloader.dataset), net.N, dtype=torch.float)

    sample_counter = 0

    with torch.no_grad():
        torch.cuda.empty_cache()

        for batch in dataloader:

            inputs = batch[0]

            if torch.cuda.is_available():
                inputs = inputs.cuda(non_blocking=True)

            outputs = net(inputs)

            outputs_max = torch.argmax(outputs, dim=1)

            for output, score in zip(outputs_max, outputs):
                predictions[sample_counter] = output
                scores[sample_counter, :] = score
                sample_counter += 1

    return predictions, scores