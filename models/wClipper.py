import torch

class MaxNorm(object):
    def __init__(self, eps=1e-8):
        self.eps = eps

    def __call__(self, model):
        for name, module in model.named_modules():
            
            if name == "depthWise1" or "classifier" in name:
                if name != "depthWise1":
                    max_val = 0.25
                else:
                    max_val = 1
                param = module.weight.data
                norm = param.norm(2, dim=0, keepdim=True)
                desired = torch.clamp(norm, 0, max_val)
                param = param * (desired / (self.eps + norm))
                module.weight.data = param