import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.init
import torchvision
import torch.nn.functional as F
import math
import random
from signals.signals import secondOrderResponse
from signals.signals import filterRLC
import os
import torch.utils.model_zoo as model_zoo







class SecondOrderSystem(nn.Module):
    def __init__(self, channelsOut, channelsIn, funcPoints, fs):

        super(SecondOrderSystem, self).__init__()

        self.channelsOut = channelsOut
        self.channelsIn = channelsIn
        self.funcPoints = funcPoints
        self.fs = fs

        self.omega = torch.FloatTensor(
            np.random.normal(self.fs / 2.0, self.fs / 6.0, self.channelsOut)
        )
        self.damping = torch.FloatTensor(
            np.random.normal(0.5, 1.0 / 6.0, self.channelsOut)
        )

        if torch.cuda.is_available():

            self.omega = self.omega.cuda(non_blocking=True)
            self.damping = self.damping.cuda(non_blocking=True)

        self.omega = nn.Parameter(self.omega)
        self.damping = nn.Parameter(self.damping)

    def forward(self, x):

        filters = torch.zeros(self.channelsOut, self.funcPoints)
        # Time vector
        t = torch.linspace(0, self.funcPoints, steps=self.funcPoints) / self.fs
        n = torch.linspace(0, self.funcPoints, steps=self.funcPoints)
        # Hamming windowing
        window = 0.54 - 0.46 * torch.cos((2 * math.pi * n) / self.funcPoints)

        if torch.cuda.is_available():
            t = t.cuda(non_blocking=True)
            n = n.cuda(non_blocking=True)
            filters = filters.cuda(non_blocking=True)
            window = window.cuda(non_blocking=True)

        for i in range(self.channelsOut):

            system = secondOrderResponse(self.omega[i], self.damping[i], t)
            filters[i, :] = (system / torch.sum(system)) * window

            if torch.cuda.is_available():
                filters[i, :] = filters[i, :].cuda(non_blocking=True)

        out = F.conv2d(
            x,
            filters.view(self.channelsOut, self.channelsIn, 1, self.funcPoints),
            padding=(0, int(self.funcPoints / 2)),
        )
        return out


class filterRLC(nn.Module):
    def __init__(self, channelsOut, channelsIn, funcPoints, fs):

        super(filterRLC, self).__init__()

        self.channelsOut = channelsOut
        self.channelsIn = channelsIn
        self.funcPoints = funcPoints
        self.fs = fs

        self.wd = torch.FloatTensor(
            np.random.normal(self.fs / 2.0, self.fs / 6.0, self.channelsOut)
        )
        self.A = torch.FloatTensor(np.random.normal(0.5, 1.0 / 6.0, self.channelsOut))
        self.A = torch.FloatTensor(np.random.normal(0.5, 1.0 / 6.0, self.channelsOut))
        self.phi = torch.FloatTensor(np.random.normal(0.5, 1.0 / 6.0, self.channelsOut))
        self.alpha = torch.FloatTensor(
            np.random.normal(0.5, 1.0 / 6.0, self.channelsOut)
        )

        if torch.cuda.is_available():

            self.wd = self.wd.cuda(non_blocking=True)
            self.A = self.A.cuda(non_blocking=True)
            self.phi = self.A.cuda(non_blocking=True)
            self.alpha = self.A.cuda(non_blocking=True)

        self.wd = nn.Parameter(self.wd)
        self.A = nn.Parameter(self.A)
        self.phi = nn.Parameter(self.phi)
        self.alpha = nn.Parameter(self.alpha)

    def forward(self, x):

        filters = torch.zeros(self.channelsOut, self.funcPoints)
        # Time vector
        t = torch.linspace(0, self.funcPoints, steps=self.funcPoints) / self.fs
        n = torch.linspace(0, self.funcPoints, steps=self.funcPoints)
        # Hamming windowing
        window = 0.54 - 0.46 * torch.cos((2 * math.pi * n) / self.funcPoints)

        if torch.cuda.is_available():
            t = t.cuda(non_blocking=True)
            n = n.cuda(non_blocking=True)
            filters = filters.cuda(non_blocking=True)
            window = window.cuda(non_blocking=True)

        for i in range(self.channelsOut):

            system = filterRLC(
                self.wd[i], self.alpha[i], self.A[i], self.phi[i], t
            )
            filters[i, :] = (system / torch.sum(system)) * window

            if torch.cuda.is_available():
                filters[i, :] = filters[i, :].cuda(non_blocking=True)

        out = F.conv2d(
            x,
            filters.view(self.channelsOut, self.channelsIn, 1, self.funcPoints),
            padding=(0, int(self.funcPoints / 2)),
        )
        return out


class ResidualBlock(nn.Module):
    def __init__(
        self,
        inChannels,
        outChannels,
        expansion,
        kernelSize,
        actFunc,
        stride=1,
        bias=False,
    ):

        super(ResidualBlock, self).__init__()

        self.inChannels = inChannels
        self.outChannels = outChannels
        self.expansion = expansion
        self.kernelSize = kernelSize
        self.stride = stride
        self.actFunc = actFunc
        self.bias = bias

        eChann = self.expansion * self.inChannels

        self.pointWise1 = nn.Conv2d(
            in_channels=self.inChannels, out_channels=eChann, kernel_size=1, bias=True
        )
        self.bn1 = nn.BatchNorm2d(eChann)
        self.convLayer = nn.Conv2d(
            in_channels=eChann,
            out_channels=eChann,
            kernel_size=self.kernelSize,
            groups=eChann,
            stride=self.stride,
            bias=self.bias,
            padding=(((self.kernelSize[0] - 1) / 2), ((self.kernelSize[1] - 1) / 2)),
        )
        self.bn2 = nn.BatchNorm2d(eChann)
        self.pointWise2 = nn.Conv2d(
            in_channels=eChann, out_channels=self.outChannels, kernel_size=1, bias=True
        )
        self.bn3 = nn.BatchNorm2d(self.outChannels)

        if self.stride > 1:
            self.res = False
        if self.inChannels != self.outChannels:
            self.downsample = nn.Conv2d(
                in_channels=self.inChannels,
                out_channels=self.outChannels,
                kernel_size=1,
                bias=True,
            )
        else:
            self.downsample = None

    def forward(self, x):

        if self.downsample:
            residual = self.downsample(x)
        else:
            residual = x

        out = self.pointWise1(x)
        out = self.actFunc(out)
        out = self.actFunc(out)
        out = self.bn1(out)

        out = self.convLayer(x)
        out = self.actFunc(x)
        out = self.bn2(x)

        out = self.pointWise2(out)
        out = self.bn3(out)

        if self.res:
            out = out + residual

        return out

    def init(self):

        nn.init.kaiming_normal_(self.pointWise1.weight)
        nn.init.kaiming_normal_(self.pointWise2.weight)
        nn.init.kaiming_normal_(self.convLayer.weight)






class Transpose(nn.Module):
    def __init__(self, dim0, dim1):

        super(Transpose, self).__init__()
        self.dim0 = dim0
        self.dim1 = dim1

        for param in self.parameters():
            param.requires_grad = False

    def forward(self, x):

        torch.cuda.empty_cache()
        x = torch.transpose(x, self.dim0, self.dim1).contiguous()

        return x


class myResNet(torchvision.models.resnet.ResNet):
    def __init__(self, net_param):

        super(myResNet, self).__init__(
            torchvision.models.resnet.BasicBlock, [2, 2, 2, 2]
        )
        # self.load_state_dict(model_zoo.load_url('https://download.pytorch.org/models/resnet18-5c106cde.pth'))

        self.act = net_param["act"]
        self.dropout = nn.Dropout(p=net_param["dropout"])
        # self.clipper = net_param["clipper"]

        if net_param.get("viewer", None):
            self.viewer = net_param["viewer"]

        self.fs = net_param["fs"]  # frequency of sampling
        self.N_sinc = net_param["sinc_filters"]  # number of sinc filters
        self.sinc_dim = net_param["sinc_size"]  # sinc filter size
        self.C = net_param["C"]  # number of channels
        # self.D = net_param["depth"]                # depth of depth-wise convolution layer
        self.T = net_param["T"]  # Time points per sample
        self.N = net_param["N"]  # Number of classes
        # self.pool_size = net_param["pool_size"]

        self.kern0 = self.C + 1 - 3
        self.kern1 = self.T + 1 - 224

        self.N_sinc = 224
        # 224xCxT
        sincConv = Sinc_conv(self.N_sinc, 1, self.sinc_dim, self.fs)
        # 1sincConv = nn.Conv2d( in_channels= 1, out_channels= self.N_sinc, kernel_size=(3,3), padding=1 )
        conv = nn.Conv2d(
            in_channels=self.N_sinc,
            out_channels=self.N_sinc,
            kernel_size=(self.kern0, self.kern1),
            stride=1,
            padding=0,
        )
        # self.resNet = torchvision.models.resnet18( pretrained= True )
        num_ftrs = self.fc.in_features
        self.fc = torch.nn.Linear(num_ftrs, self.N)

        lnorm = nn.LayerNorm([self.N_sinc, self.C, self.T])
        bn = nn.BatchNorm2d(3)

        nn.init.xavier_normal_(conv.weight)

        nn.init.xavier_normal_(self.fc.weight)

        self.conv1 = nn.Sequential(
            sincConv,
            self.act,
            lnorm,
            self.dropout,
            conv,
            Transpose(1, 2),
            self.act,
            bn,
            self.dropout,
            self.conv1,
        )


"""
    def forward(self,x):

        x = self.sincConv( x )
        
        x = self.act( x )
        x = self.lnorm( x )
        x = self.dropout( x )

        x = self.conv( x )

        x = torch.transpose(x,1,2)
        x = x.contiguous()


        print x.shape

        x = self.act( x )
        x = self.bn( x )
        x = self.dropout( x )

        x = self.resNet( x )

        return x
"""


class SincConvNet(nn.Module):
    def __init__(self, net_param):

        super(SincConvNet, self).__init__()

        self.act = net_param["act"]
        self.dropout = nn.Dropout(p=net_param["dropout"])
        # self.clipper = net_param["clipper"]

        if net_param.get("viewer", None):
            self.viewer = net_param["viewer"]

        self.fs = net_param["fs"]  # frequency of sampling
        self.N_sinc = net_param["sinc_filters"]  # number of sinc filters
        self.sinc_dim = net_param["sinc_size"]  # sinc filter size
        self.C = net_param["C"]  # number of channels
        # self.D = net_param["depth"]                # depth of depth-wise convolution layer
        self.T = net_param["T"]  # Time points per sample
        self.N = net_param["N"]  # Number of classes
        # self.pool_size = net_param["pool_size"]

        self.kernelSize = (3, 5)
        self.poolSize = (1, 2)

        self.sincConv = Sinc_conv(self.N_sinc, 1, self.sinc_dim, self.fs)
        self.pool1 = nn.MaxPool2d(kernel_size=self.poolSize, stride=self.poolSize)
        self.lnorm1 = nn.LayerNorm(
            [self.N_sinc, self.C / self.poolSize[0], self.T / self.poolSize[1]]
        )

        self.depthWiseConv = nn.Conv2d(
            in_channels=self.N_sinc, out_channels=self.N_sinc, kernel_size=(self.C, 1)
        )
        self.lnorm2 = nn.LayerNorm([self.N_sinc, 1, self.T / self.poolSize[1]])

        self.conv1 = nn.Conv2d(
            in_channels=1,
            out_channels=5,
            kernel_size=self.kernelSize,
            stride=(self.kernelSize[0], 1),
        )
        self.pool2 = nn.MaxPool2d(kernel_size=self.poolSize, stride=self.poolSize)
        newT = (((self.T) / self.poolSize[1]) - self.kernelSize[1] + 1) / self.poolSize[
            1
        ]
        self.lnorm3 = nn.LayerNorm([5, self.N_sinc / self.kernelSize[0], newT])

        self.conv2 = nn.Conv2d(
            in_channels=5,
            out_channels=10,
            kernel_size=self.kernelSize,
            stride=(self.kernelSize[0], 1),
        )
        self.pool3 = nn.MaxPool2d(kernel_size=self.poolSize, stride=self.poolSize)
        self.newT = (newT - self.kernelSize[1] + 1) / self.poolSize[1]
        self.lnorm4 = nn.LayerNorm(
            [10, self.N_sinc / self.kernelSize[0] ** 2, self.newT]
        )

        self.fc1 = nn.Linear(
            in_features=(self.newT * 10) * int(self.N_sinc / self.kernelSize[0] ** 2),
            out_features=500,
        )
        self.bn1 = nn.BatchNorm1d(num_features=500)
        self.fc2 = nn.Linear(in_features=500, out_features=50)
        self.bn2 = nn.BatchNorm1d(num_features=50)
        self.classifier = nn.Linear(in_features=50, out_features=self.N)

        self.sig = nn.Sigmoid()

        nn.init.xavier_normal_(self.depthWiseConv.weight)
        nn.init.xavier_normal_(self.conv1.weight)
        nn.init.xavier_normal_(self.conv2.weight)
        nn.init.xavieshaper_normal_(self.fc1.weight)
        nn.init.xavier_normal_(self.fc2.weight)
        nn.init.xavier_normal_(self.classifier.weight)

    def forward(self, x):

        x = self.sincConv(x)
        x = self.pool1(x)
        x = self.act(x)
        x = self.lnorm1(x)
        x = self.dropout(x)

        x = self.depthWiseConv(x)
        x = self.act(x)
        x = self.lnorm2(x)
        x = self.dropout(x)

        # print x.shape

        x = torch.transpose(x, 1, 2)
        x = x.contiguous()

        # print x.shape

        x = self.conv1(x)
        # print x.shape
        x = self.pool2(x)
        x = self.act(x)
        x = self.lnorm3(x)
        x = self.dropout(x)

        x = self.conv2(x)
        x = self.pool3(x)
        x = self.act(x)
        x = self.lnorm4(x)
        x = self.dropout(x)

        # print x.shape
        # print self.newT
        # print self.N_sinc
        # print

        x = x.view(-1, (self.newT * 10) * int(self.N_sinc / self.kernelSize[0] ** 2))

        x = self.fc1(x)
        x = self.act(x)
        x = self.bn1(x)
        x = self.dropout(x)

        x = self.fc2(x)
        x = self.act(x)
        x = self.bn2(x)
        x = self.dropout(x)

        x = self.classifier(x)

        return x


class MobileSinc(nn.Module):
    def __init__(self, netParams):

        super(MobileSinc, self).__init__()
        self.act = netParams["act"]
        self.dropout = nn.Dropout(p=netParams["dropout"])
        self.fs = netParams["fs"]  # frequency of sampling
        self.nSinc = netParams["sinc_filters"]  # number of sinc filters
        self.sinc_dim = netParams["sinc_size"]  # sinc filter size
        self.C = netParams["C"]  # number of channels
        self.D = netParams["depth"]  # depth of depth-wise convolution layer
        self.T = netParams["T"]  # Time points per sample
        self.N = netParams["N"]  # Number of classes

        self.convLayer = SincConv2D(
            channelsOut=self.nSinc,
            channelsIn=1,
            timePts=7,
            channPts=7,
            fs=self.fs,
            stride=(1, 2),
        )
        self.bn1 = nn.BatchNorm2d(self.nSinc)
        # nSincx64x192
        self.block1 = ResidualBlock(
            inChannels=self.nSinc,
            outChannels=64,
            expansion=self.D,
            kernelSize=(5, 5),
            actFunc=self.act,
            stride=(2, 2),
            bias=True,
        )
        # 64x32x96
        self.block2 = ResidualBlock(
            inChannels=64,
            outChannels=96,
            expansion=self.D,
            kernelSize=(5, 5),
            actFunc=self.act,
            stride=(2, 2),
            bias=True,
        )
        # 96x16x48

        self.resBlock1 = ResidualBlock(
            inChannels=96,
            outChannels=96,
            expansion=self.D,
            kernelSize=(5, 5),
            actFunc=self.act,
            stride=(1, 1),
            bias=True,
        )

        self.block3 = ResidualBlock(
            inChannels=96,
            outChannels=160,
            expansion=self.D,
            kernelSize=(5, 5),
            actFunc=self.act,
            stride=(2, 2),
            bias=True,
        )
        # 160x8x24
        self.block4 = ResidualBlock(
            inChannels=160,
            outChannels=320,
            expansion=self.D,
            kernelSize=(5, 5),
            actFunc=self.act,
            stride=(2, 2),
            bias=True,
        )
        self.resBlock2 = ResidualBlock(
            inChannels=320,
            outChannels=320,
            expansion=self.D,
            kernelSize=(5, 5),
            actFunc=self.act,
            stride=(1, 1),
            bias=True,
        )
        # 320x4x12
        self.pool = nn.AvgPool2d(kernel_size=(4, 12))
        self.classifier = nn.Conv2d(in_channels=320, out_channels=self.N, kernel_size=1)

        self.block1.init()
        self.block2.init()
        self.block3.init()
        self.block4.init()

    def forward(self, x):

        x = self.convLayer(x)
        x = self.act(x)
        x = self.bn1(x)
        x = self.dropout(x)

        x = self.block1(x)
        x = self.block2(x)
        # x = self.resBlock1( x )

        x = self.block3(x)
        x = self.block4(x)
        x = self.resBlock2(x)

        x = self.pool(x)
        x = self.classifier(x)

        x = x.view(-1, self.N)

        return x
