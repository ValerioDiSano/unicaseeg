import json
import sys
import os


def pars_parameters():
    """Parser for JSON files

    Parse a JSON file whose path has been specified from the command line

    Returns
    -------
    dictionary containing JSON file variables 

    """

    with open(sys.argv[1]) as f:

        args = json.load(f)

    for k, v in args.items():
        print k, v

    return args



def recursiveVisit(dirs, ext, verbose=False):
    """ Explore recursivly a collection of paths, keeping only the filepaths with a given exstension.
    
    Parameters
    ----------
    dirs : iterable object
        collection of paths to explore
    ext : str or collection of str
        file extensions to keep
    
    Returns
    --------
    list with all the found files
    """
    if dirs:
        files = []
        for dir in dirs:
            dirFiles = os.listdir(dir)
            dirFiles = [os.path.join(dir, f) for f in dirFiles]
            files += dirFiles

        currentDirs = [f for f in files if os.path.isdir(f)]
        currentFiles = [f for f in files if f.endswith(ext)]

        if verbose:
            print "In folders {} these files are found with extension: {}".format(
                dirs, ext
            )
            print currentFiles
            print "And these subfolders:"
            print currentDirs

    else:
        return []

    return currentFiles + recursiveVisit(currentDirs, ext)


def getFolderPath(path):

    for i in range(len(path)):

        if path[-(i + 1)] == os.path.sep:
            return path[: -(i + 1)]

    return ""


def isNan(num):
    """ Check if a class is equal to Not a Number
    
    Parameters
    ---------
    num : object
        object to check

    Returns
    ---------
    True if num is equal to Nan, False otherwise
    """

    return num != num





