import torch.nn as nn


def activation_layer(act_name, _inplace=True):
    """ This function allows to select an activation layer from a given set

    Parameters
    ----------
    act_name : str
        name of the wanted activation layer
    _inplace : bool, optional
        if true the selected function makes computations in place (default is True)
    
    Returns
    ---------
    activation layer : nn.Module
        CELU is returned by default
    """

    act_name = act_name.lower()
    print act_name

    switcher = {
        "celu": nn.CELU(inplace=_inplace),
        "lrelu": nn.LeakyReLU(inplace=_inplace),
        "elu": nn.ELU(inplace=_inplace),
        "rrelu": nn.RReLU(inplace=_inplace),
        "prelu": nn.PReLU(),
        "selu": nn.SELU(inplace=_inplace),
        "relu": nn.ReLU(inplace=_inplace),
        "tanh": nn.Tanh(),
        "relu6": nn.ReLU6(inplace=_inplace),
    }

    return switcher.get(act_name, switcher["celu"])


def device_id(dev_numb):
    """ It associates a PyTorch device ID to a sequential GPU number

    Parameters
    ----------
    dev_numb : int
        GPU sequential number
    
    Returns
    ---------
        PyTorch GPU ID
    """

    return "cuda:" + str(dev_numb)
