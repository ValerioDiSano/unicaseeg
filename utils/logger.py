import datetime
import os
import matplotlib

matplotlib.use("Agg") # Necessary in order to run the code on a machine without a graphical environment

import matplotlib.pyplot as plt



class Logger:

    """ Logger class writes experiment statistics to disk
        
        Args
        ------------
        path : string
            the class is instantiated on the top of this path
        experiment_ID : string
            experiment identifier, it's used as name of the produced txt file
        append : bool
            indicates whether to create a new file anyway or to append if a log file already exists

        Attributes
        -----------
        experiment_folder : string
            path to directory where to save all the files produced
            computed as <year>_<month>_<day>_<hour>
        path : string
            the class is instantiated on the top of this path
        log_file : string
            filename of the txt file in which the information is stored
        log_reference : object
            file object refering to log_file
    """

    def __init__(self, path, experiment_ID, append=False):

        self.path = path
        self.fold_num = 0

        now = datetime.datetime.now()
        self.experiment_folder = "_".join(
            [str(now.year), str(now.month), str(now.day), str(now.hour)]
        )
        self.experiment_folder = os.path.join(path, self.experiment_folder)

        # Check if the experiment folder already exists
        idx = 0
        base_str = self.experiment_folder
        while True:
            if not os.path.exists(self.experiment_folder):
                os.makedirs(self.experiment_folder)
                break
            else:
                idx += 1
                self.experiment_folder = "__num__".join([base_str, str(idx)])

        self.log_file = os.path.join(self.experiment_folder, experiment_ID + ".log")

        # Define the log file opening mode
        if append:
            mode = "a"
        else:
            mode = "w"

        self.log_reference = open(self.log_file, mode)

        self.log_reference.write(experiment_ID + "\n\n")  # Save experiment ID
        self.log_reference.write(
            "Date / hour : "
            + str(now.year)
            + " "
            + str(now.month)
            + " "
            + str(now.day)
            + "/"
            + str(now.hour)
            + "\n"
        )  # Save data and hour

    def update_train(self, n_epoch, loss, train_accuracy, valid_accuracy, elapsed_time):
        """ Update the experiment training phase statistics when a an epoch is completed

            Args:
                n_epoch( int ): number of the completed epoch
                loss( float ): current loss function value
                train_accuracy( float ): accuracy achieved on the training set during the epoch
                valid_accuracy( float ): accuracy achieved on the validation set during the epoch
                elapsed_time ( float ): time spents to complete the epoch

        """
        if n_epoch == 1:
            self.fold_num += 1

            if not os.path.exists(
                os.path.join(self.experiment_folder, "FOLD_" + str(self.fold_num))
            ):
                os.makedirs(
                    os.path.join(self.experiment_folder, "FOLD_" + str(self.fold_num))
                )

            self.log_reference.write("\n\n\nFOLD %d\n" % self.fold_num)
            self.log_reference.write("\nTRAINING PHASE \n")

        self.log_reference.write(
            "Epoch [%d], \t Loss: %f, \t Training acc: %f, \t Validation acc: %f, \t Elapsed time: %f s \n"
            % (n_epoch, loss, train_accuracy, valid_accuracy, elapsed_time)
        )

        # Force OS to complete the writing process
        self.log_reference.flush()
        os.fsync(self.log_reference.fileno())

    def test_stats(self, epoch, valid_acc, test_accuracy, time = 0.0):
        """ Save the performance evaluation based on the test set

            Args
            -------
            epoch : int
                epoch in which the used model has been computed
            accuracy : float
                accuracy on the test set

        """

        self.log_reference.write("\n TEST PHASE \n")
        self.log_reference.write(
            " Best model achieved at epoch %d with validation accuracy %f\n"
            % (epoch, valid_acc)
        )
        self.log_reference.write("Test accuracy: %f \n" % (test_accuracy, ))
        self.log_reference.write("Inference time: %f \n" % (time, ))

    def stats_plot(self, ticks, train_accuracies, valid_accuracies, loss_fcn):
        """ Save the plots of the accuracy on the train and validation set and the loss function over the number of epoch

            Args
            --------
            ticks : list(int)
                ticks for the x axis of the plot
            train_accuracies : list(float)
                train accuracies over the epochs
            valid_accuracies : list(float)
                validation accuracies over the epochs
            loss_fcn : list(float)
                loss function value over the epochs
        """

        plt.plot(ticks, loss_fcn)

        plt.savefig(
            os.path.join(
                self.experiment_folder, "FOLD_" + str(self.fold_num), "loss_fcn.png"
            )
        )

        plt.clf()

        # Plot on the same chart the validation and the training accuracy
        fig, ax = plt.subplots()

        ax.plot(ticks, train_accuracies, "r", label="training acc")
        ax.plot(ticks, valid_accuracies, "b", label="validation acc")
        ax.legend(loc="upper left")
        fig.savefig(
            os.path.join(
                self.experiment_folder,
                "FOLD_" + str(self.fold_num),
                "accuracy_plot.png",
            )
        )

        fig.clf()

    def auc_roc(self, auc):
        """ For each class save the AUC ( Area Under the Curve ) value linked to the ROC curve

            Args:
                auc ( dict{ class:auc(float) } ): dictionary in which to each key ( class number ) is associated the auc value
        """
        for i in range(len(auc)):
            self.log_reference.write("\n AUC class %d : %f  \n" % (i, auc[i]))

    def print_parameters(self, d):

        self.log_reference.write("\nParameters list:\n")
        for k, v in d.items():
            self.log_reference.write(str(k) + ": ")
            self.log_reference.write(str(v) + "\n")
        self.log_reference.write("\n\n")

    def fold_stats(self, validation, test, auc):
        self.log_reference.write("\n\n")
        mean_valid = np.mean(validation.values())
        mean_test = np.mean(test.values())
        mean_auc = np.mean(auc.values(), axis=0)
        for i in range(len(validation)):
            self.log_reference.write(
                "Fold %d, validation acc: %f; test acc: %f \n"
                % (i + 1, validation[i + 1], test[i + 1])
            )
        self.log_reference.write("Mean validation acc: %f \n" % (mean_valid,))
        self.log_reference.write("Mean test acc: %f \n\n" % (mean_test,))
        for j in range(len(mean_auc)):
            self.log_reference.write("Mean AUC class %d: %f \n" % (j, mean_auc[j]))