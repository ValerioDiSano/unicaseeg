import matplotlib

matplotlib.use("Agg") # Necessary in order to run the code on a machine without a graphical environment

import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import numpy as np
import os

class ROC:
    """
        ROC class is built on the top of the skleran roc module
        in order to provive a ROC curve for each class contained
        in the data in an immediate way

        Args
        -----------
        n_class :int
            number of classes contained in the data
        Attributes
        -----------
        n_class : int
            number of classes contained in the data
        fpr : dict{ class: false positive rate }
            dictonary that associate to each calss the number of false positive rates
        tpr : dict{ class: true positive rate }
            dictonary that associate to each calss the number of true positive rates
    """

    def __init__(self, n_class):

        self.n_class = n_class

        self.fpr = dict()
        self.tpr = dict()

        self.roc_auc = dict()

    def compute_ROC(self, y_test, y_score):
        """ Given the results on the test set compute the ROC curve for all the classes

            Args
            --------
            y_test : iterable(int)
                label for each sample of the test set
            y_score : iterable(float x 2)
                classification score for each sample and for each class

        """

        for i in range(self.n_class):
            class_labels = np.zeros(len(y_test))
            for j in range(len(y_test)):
                if y_test[j] == i:
                    class_labels[j] = 1

            # Compute AUC
            self.fpr[i], self.tpr[i], _ = roc_curve(class_labels, y_score[:, i])
            self.roc_auc[i] = auc(self.fpr[i], self.tpr[i])

    def save_all(self, path):
        """ save the ROC curve for each class a png image

            Args
            -----
            path : string
                path to the folder where to save the figures

        """

        if not os.path.exists(path):
            os.makedirs(path)

        for i in range(len(self.roc_auc)):

            plt.plot(self.fpr[i], self.tpr[i], label="ROC curve class %d" % (i,))
            plt.savefig(os.path.join(path, str(i) + "_roc.png"))
            plt.clf()
