import scipy
from scipy.signal import resample_poly
import numpy as np
from sklearn.preprocessing import power_transform
from sklearn.preprocessing import robust_scale
from scipy.linalg import fractional_matrix_power
import scipy.io
import mne.filter as filt
from sklearn.decomposition import FastICA
from dictionaries import dict_MT

def FIR_downsampling(x, old_fs, new_fs, axis):
    """ Apply downsampling to a signal using FIR filter

        Parameters
        ----------
        x : iterable
            signal to process
        old_fs : int
            sample rate of the input signal
        new_fs : int
            new sampling rate of the signal after the processing
        
        Returns
        ---------
        downsampled version of the signal
    """
    x = resample_poly(
        x, up=new_fs, down=old_fs, window=("general_gaussian", 1, 40), axis=axis, verbose= False
    )

    return x


class OneHotEncode(object):
    """ OneHotEncode class used to apply the One Hot encoding 

        Attributes:
        n_class : int
            number of classes
        
        Methods:
        __call__
            apply the encoding
    """

    def __init__(self, n_class):
        self.n_class = n_class

    def __call__(self, label):
        target = torch.zeros(self.n_class, dtype=torch.float)
        target[label] = 1.0
        return target


def dict_normMeanStd(X):

    mean = np.mean(X.values())
    std = np.std(X.values())
    X = {key: (value - mean) / std for (key, value) in X.items()}

    return X


def dict_normTanh(X):
    """Tanh normalization of a dictionary
    """

    mean = np.mean(X.values())
    std = np.std(X.values())
    X = {
        key: 0.5 * np.tanh((0.01 * (value - mean) / std) + 1)
        for (key, value) in X.items()
    }

    return X


def dict_ZCA(X, T=None, PCA=False):
    """ZCA/PCA whitening

    Parameters
    -----------
    X : dict
    
    T : transformation, default None
        if None the transformation is estimated on the data otherwise is only applied
    PCA : bool
        if True apply the PCA transformation
    """

    print "ZCA"

    dim = len(X)

    C = X[0].shape[0]
    N = X[0].shape[1]
    mean = np.zeros([C, N])

    if T is None:

        # mean = np.mean( X.values() )
        for k in X.keys():
            mean = mean + X[k]

        mean = np.divide(mean, dim)
        mean = np.mean(mean, axis=1)
        Cov = 0
        for k in X.keys():

            # mean = np.mean( X[k], axis=1 )
            Xcenter = np.subtract(np.transpose(X[k]), mean).transpose()
            Cov += np.matmul(Xcenter, np.transpose(Xcenter))
            Cov /= C - 1

        Cov /= dim - 1

        U, S, _ = scipy.linalg.svd(Cov)

        if not PCA:
            T = np.matmul(
                np.matmul(U, scipy.linalg.fractional_matrix_power(np.diag(S), -0.5)),
                np.transpose(U),
            )
        else:
            T = np.matmul(
                (scipy.linalg.fractional_matrix_power(np.diag(S), -0.5)),
                np.transpose(U),
            )

    X = {key: np.matmul(T, value) for (key, value) in X.items()}

    return X, T


def dict_FIR(X, fs, lowF=0.0, highF=65):
    """Finite Impulse Renspose bandpass fitlering

    X : dict
        data to filter
    fs : float
        frequency of sampling of the data
    lowF : float
        lower cut-off frequency
    highF : float
        higher cut-off frequency

    """

    X = {key: (filt.filter_data(value, fs, lowF, highF, verbose= False)) for (key, value) in X.items()}

    return X


def dict_resample(X, newFs, oldFs):

    X = {
        key: (filt.resample(value, up=newFs, down=oldFs, npad="auto"))
        for (key, value) in X.items()
    }
    # X = { key:( np.transpose(filt.resample(np.transpose(value),up = 16,down= 64,npad="auto" ) )) for (key,value) in X.items() }

    return X


def dict_clip(X, lowClip=-0.2, highClip=0.2):

    for key in X.keys():
        np.clip(X[key], lowClip, highClip, X[key])


def dict_medianFilt(X):

    print "MEDIAN FILTERING"
    for key in X.keys():

        X[key] = scipy.signal.medfilt(X[key], kernel_size=[1, 3])

    return X


def dict_FirResample(X, newFs, oldFs, lowF=0, highF=40):

    X = dict_resample(dict_FIR(X, oldFs, lowF=lowF, highF=highF), newFs, oldFs)

    return X


def dict_MeanStdNorm(X, Cov=None, mean=None):
    print "meanSTDNORM"
    if (Cov is None) or (mean is None):
        C = X[X.keys()[0]].shape[0]
        T = X[X.keys()[0]].shape[1]
        N = len(X)
        mean = np.zeros((C, T))
        for key in X.keys():
            mean += X[key].astype(float)
        mean = np.divide(mean, N)
        mean = np.mean(mean, axis=1)

        Cov = 0
        for k in X.keys():

            XCenter = np.subtract(np.transpose(X[k]), mean).transpose()

            Cov += np.matmul(XCenter, np.transpose(XCenter))
            Cov /= C - 1

        Cov /= N - 1

        print Cov.shape

    sigma = scipy.linalg.fractional_matrix_power(Cov, -0.5)
    for key in X.keys():
        XCenter = np.subtract(np.transpose(X[key]), mean).transpose()
        X[key] = np.array(np.matmul(sigma, XCenter)).astype(float)

    # if path:
    #    saveDataDict(X,path)

    return X, Cov, mean


def dict_normDataset(X, mean=None, std=None):

    print "Norm dataset"
    data = np.array([X.pop(key) for key in X.keys()])
    print type(data)
    print data.shape

    if mean is None or std is None:
        mean = data.mean(axis=(0, 2), keepdims=True)
        # mean = np.sum( np.reshape(np.sum( data, axis= 0), [ data.shape[1], data.shape[0] ] ), axis=1 )
        std = data.std(axis=(0, 2), keepdims=True)

    data = (data - mean) / std

    X = {k: data[k, :, :] for k in range(data.shape[0])}
    return X, mean, std


def dict_MeanStdPerSample(X):

    print "Per sample"
    for key in X.keys():

        X[key] = normalize(X[key])

    return X


def normalize(sample):
    #print "normalize mean and std"
    mean = np.mean(sample, axis=1)
    sample = (np.subtract(np.transpose(sample), mean)).transpose()
    # print mean.shape
    # sample = np.divide(np.transpose(sample),np.std(sample,axis=1)).transpose()

    # sample = np.divide( np.transpose(sample), np.divide(np.linalg.norm( sample, ord=2, axis=1), np.sqrt(sample.shape[1] - 1) ) ).transpose()

    cov = np.matmul(sample, np.transpose(sample))
    cov /= sample.shape[0] - 1

    # U,S,_ = scipy.linalg.svd( cov )

    # T = np.matmul( U, np.matmul( scipy.linalg.fractional_matrix_power( np.diag(S), -0.5 ), np.transpose(U) ))
    # sample = np.matmul(T,sample)
    sample = np.matmul(scipy.linalg.fractional_matrix_power(cov, -0.5), sample)

    return sample


def saveDataDict(X, path):
    """Save a dictionary on the Hard Drive
    """

    if not os.path.isdir(getFolderPath(path)):
        os.makedirs(getFolderPath(path))
    X_save = np.zeros([len(X), X[0].shape[0], X[0].shape[1]])
    for idx, key in enumerate(sorted(X.keys())):
        X_save[idx, :, :] = X[key]
    X_save = {"X": X_save}
    scipy.io.savemat(path, X_save)



def ICA(*args):

    print "ica"

    args = enrollTuple(args)
    sample = args[0]
    transform = args[1]

    sample = transform.fit_transform(np.transpose(sample))
    sample = np.transpose(sample)
    return sample


def dict_ICA(X):
    """Independent Component Analysis
    """

    print "DICT ICA"
    transform = FastICA(n_components=X[0].shape[0])
    func = ICA

    X = dict_MT(X, func, (transform,))

    return X


def dict_normPerSample(X):
    """ Normalize data in a multithreading fashion
    """

    print "DICT NORM PER SAMPLE"

    func = normalize

    X = dict_MT(X, func)

    return X


def dict_PowerTransform(X):
    """Applies the power transform(Yeo-Johnson) operator on the values of a dictionary in Multi Threading
    """

    print "POWER Transform"

    func = lambda x: np.transpose(
        power_transform(np.transpose(x), method="yeo-johnson", copy=False)
    )

    X = dict_MT(X, func)

    return X


def dict_PrimeDifference(X):
    """Applies the prime difference operator on the values of a dictionary in Multi Threading
    """

    print "Prime DIfference"

    func = lambda x: x - np.roll(x, -1)

    X = dict_MT(X, func)

    return X


def dict_RobustScale(X):
    """Robust scaling on the values of a dictionary in Multi Threading
    """

    print "Robust scaling"

    func = lambda x: robust_scale(np.transpose(x)).transpose()

    X = dict_MT(X, func)

    return X


def dict_MinMaxScale(X):
    """Min Max scaling on the values of a dictionary in Multi Threading
    """

    func = lambda x: (x - np.amin(x, axis=0)) / (
        np.amax(x, axis=0) - np.amin(x, axis=0)
    )

    X = dict_MT(X, func)

    return X
