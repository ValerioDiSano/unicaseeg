import threading
import multiprocessing
import os
import numpy as np
import scipy
import scipy.io

from utils.others import getFolderPath
from utils.dataOp import enrollTuple

def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    priority goes to key value pairs in latter dicts.

    Parameters:
    -----------
    dict_args : set of dicts
    
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def dict_MT(X, func, *args):
    """ Interface used to apply a function to each element of a dictionary in multithreading

    Paramters
    ----------
    X : dict

    func : function handler
    
    *args : additional function arguments

    """

    cond = threading.Condition()

    for key in X.keys():

        cond.acquire()
        cat_args = enrollTuple((X[key],) + args)

        X[key] = dict_MultiThread(func, cond, (cat_args))
        X[key].start()

        while threading.activeCount() >= multiprocessing.cpu_count():
            cond.wait()
        cond.release()

    for i in X.keys():
        X[i].join()
        X[i] = X[i].sample

    return X


class dict_MultiThread(threading.Thread):
    def __init__(self, func, cond, *args):

        super(dict_MultiThread, self).__init__()

        self.func = func
        self.cond = cond
        self.sample = 0
        self.args = enrollTuple(args)

        # print "Thread {} spawns".format((threading.current_thread().ident))
        #print "Thread {} spawns".format((multiprocessing.Process().pid))

    def run(self):

        if len(self.args) > 1:
            self.sample = self.func(self.args)
        else:
            self.sample = self.func(self.args[0])
        self.cond.acquire()
        self.cond.notify()
        self.cond.release()


    

def dictSubset(X, subSize, intervals):
    """Extract a subdictionary
    """

    subX = {}

    updataLow = (
        lambda lowBound, offset, k: lowBound - k * offset if lowBound else lowBound
    )
    updataHigh = (
        lambda highBound, lowBound, offset, k: highBound - k * 2 * offset
        if lowBound
        else highBound - offset
    )

    # nSamples = int( subSize / len(intervals) )
    nSamples = 3
    for k, (pat, interval) in enumerate(intervals.items()):
        idxList = []
        for _ in range(nSamples):
            while True:
                idx = randint(interval[0], interval[1])
                if idx not in idxList:
                    break
            idxList.append(idx)
            subX[idx] = X.pop(idx)

        intervals[pat] = (
            updataLow(interval[0], nSamples, k),
            updataHigh(interval[1], interval[0], nSamples, k),
        )

    return subX, X, intervals


def saveDataDict(X, path):

    if not os.path.isdir(getFolderPath(path)):
        os.makedirs(getFolderPath(path))
    X_save = np.zeros([len(X), X[0].shape[0], X[0].shape[1]])
    for idx, key in enumerate(sorted(X.keys())):
        X_save[idx, :, :] = X[key]
    X_save = {"X": X_save}
    scipy.io.savemat(path, X_save)


