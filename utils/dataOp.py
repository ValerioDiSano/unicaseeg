from random import randint
import numpy as np
import math



def random_subdictionary(dictIn, subLen):
    """Split a dictionary in two subdictionary randomly

    Parameters
    ----------
    dictIn : dict
        dictionary to split
    subLen : int
        length of the generated dictionary
    
    Returns:
    dictOut : dict
        extracted dictionary
    dictIn : dict
        input dictionary without elements present in dictOut
    """

    dictOut = {}

    for _ in range(subLen):
        randIdx = randint(0, len(dictIn) - 1)
        key = dictIn.keys()[randIdx]

        dictOut[key] = dictIn.pop(key)

    return dictOut, dictIn


def compute_balance_weights(labels):
    """Computes ratios between classes in a Dataset

    Parameters
    ----------
        labels : iterable
            labels of the dataset
    Returns
    ---------
        dictionary with a weight for each class
    """

    weights = dict()
    n_class = max(labels) + 1
    for i in range(int(n_class)):
        weights[i] = labels.count(i) / float(len(labels))

    print weights

    max_val = max(weights.values())

    weights = dict((key, int(max_val / value)) for key, value in weights.iteritems())

    print weights

    return weights




def divHalf_odd(x):
    """ Computes the closest odd integer to the half of the input

    Parameters
    ----------
    x : int or float
    
    Returns
    ---------
    int
    """

    approx = [math.ceil, math.floor]

    x = x / 2.0

    nearInt = np.argmin([approx[0](x) - x, approx[1](x) - x])

    if approx[nearInt](x) % 2:
        return int(approx[nearInt](x))
    elif approx[not nearInt](x) % 2:
        return int(approx[not nearInt](x))
    else:
        return int(x + 1)


def freq2mel(freq):
    """Converts a frequency [Hz] in the Mel scale
    """

    return 2595 * math.log10(1 + (freq / 700.0))


def mel2freq(mel):
    """Converts a value in the Mel scale in a frequency [Hz]
    """

    return 700 * (10 ** (mel / 2595.0) - 1)


def load_labels(path):
    """Read a file containing float numbers.
    
    It is useful to read text files containing numeric data labels.

    Parameters
    ----------
    path : str
        path to the file to read
    
    Returns
    ---------
        numpy array with the numbers of the file
    """

    with open(path, "r") as f:
        labels = map(float, f)

    return np.array(labels)


def getFolderPath(path):

### Returns the name of the last directory in a given path

    for i in range(len(path)):
        if path[-(i + 1)] == os.path.sep:
            return path[: -(i + 1)]

    return ""



def underSampling(X, y, sets):

    Xs = {}
    yS = {}

    minS = float("inf")
    for pat, interv in sets.items():
        nSamples = (interv[1] - interv[0]) + 1
        print "Patience {} has got {} samples".format(pat, nSamples)
        if nSamples < minS:
            minS = nSamples

    for set in sets.values():
        idxList = []
        print set
        setSize = set[1] - set[0] + 1

        if setSize == minS:
            print setSize
            continue

        for k in range(minS):
            print k
            while True:
                idx = randint(set[0], set[1])
                if idx in idxList:
                    continue

                Xs[len(Xs)] = X[idx]
                yS[len(yS)] = y[idx]
                idxList.append(idx)
                break
    return Xs, yS


def samplingOnPatiences(X, y, sets):
###Super sampling on data in order to have the same number of samples for each patience in the dataset

    lastId = len(X)

    maxS = 0
    for pat, interv in sets.items():
        nSamples = (interv[1] - interv[0]) + 1
        print "Patience {} has got {} samples".format(pat, nSamples)
        if nSamples > maxS:
            maxS = nSamples

    for interv in sets.values():
        sizeI = interv[1] - interv[0] + 1

        if sizeI == maxS:
            continue
        print maxS
        print int(maxS - sizeI)
        for k in range(int(maxS - sizeI)):
            idx = randint(interv[0], interv[1])
            X[lastId + k] = X[idx]
            y[lastId + k] = y[idx]
        lastId = lastId + k + 1

    return X, y

def enrollTuple(T):
    """Check if inside a Tuple object other tuples are present. In this case inner tuples are enrolled

    Paramters
    ----------
    T : tuple
    """

    T_ = tuple()
    for tt in T:
        if isinstance(tt, tuple):
            for t in tt:
                T_ += (t,)
        else:
            T_ += (tt,)

    return T_