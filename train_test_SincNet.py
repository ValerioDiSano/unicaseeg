#!/usr/bin/env python2

import time
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms

from BCI_data.competition_III_1 import competition_III_1
from BCI_data.competition_III_3a import competition_III_3a
from BCI_data.competition_III_4a import competition_III_4a
from BCI_data.competition_IV import competition_IV

from BCI_data.transforms import Convert_1D
from BCI_data.transforms import Label_decrement
from BCI_data.transforms import LabelMap

from models.sincNet import SincNet
from models.wClipper import MaxNorm
from models.sincLayer import SincViewer

import os
import datetime

from utils.roc import ROC
from utils.netUtils import activation_layer, device_id
from utils.dataOp import compute_balance_weights
from utils.logger import Logger
from utils.dictionaries import merge_dicts
from utils.others import pars_parameters

def main():

    param = pars_parameters()  # Retrieve command line parameters

    sampleTransformT = torchvision.transforms.Compose(
        [
            Convert_1D()
        ]
    )

    if param["dataset"] == "comp_I":
        # data = BCI_data.competition_I(preprocess=param["prep"],k= k_fold, norm= param["norm"])
        data = competition_III_1(
            labelTransform=LabelMap(),
            sampleTransformTrain=sampleTransformT,
            sampleTransform=Convert_1D(),
            preprocessing=param["prep"],
        )
    elif param["dataset"] == "comp_IV":
        data = competition_IV(
            sample_transform=Convert_1D(),
            label_transform=Label_decrement(),
            preprocessing=param["prep"],
        )

    #k_fold = param["k_fold"]
    # Network Hyperparameters

    batch_size = param["batch_size"]
    learning_rate = param["learning_rate"]
    max_epochs = param["epochs"]
    weight_decay = param["weight_decay"]
    #lr_step_size = param["step_size"]
    #lr_gamma = param["gamma"]

    # Network's layers parameters
    act = activation_layer(param["activation"])

    viewer = SincViewer()

    device = device_id(param["device"])
    torch.cuda.set_device(param["device"])
    num_workers = 10

    net_param = merge_dicts(
        {
            k: param[k]
            for k in (
                "dropout",
                "sinc_filters",
                "sinc_size",
                "conv_depth",
                "conv_size",
                "pool_size",
                "linear_units",
            )
        },
        {k: data[k] for k in ("fs", "T", "N", "C")},
        {"act": act, "viewer": viewer},
    )

    # Create network
    net = SincNet(net_param)

    if torch.cuda.is_available():
        # Network to GPU
        net.to(device)

    ### Loss function ###
    if param["weights"]:
        w = torch.FloatTensor(
            compute_balance_weights(data["train"].get_labels()).values()
        )
        if torch.cuda.is_available():
            w = w.cuda()
    else:
        w = None

    ### Loss function ###
    criterion = nn.CrossEntropyLoss(weight=w)

    ### Optimizer ###
    if param["optim"] == "adam":
        optimizer = optim.Adam(
            net.parameters(), lr=learning_rate, amsgrad=True, weight_decay=weight_decay
        )
    elif param["optim"] == "rmsprop":
        optimizer = optim.RMSprop(
            net.parameters(), lr=learning_rate, weight_decay=weight_decay
        )
    elif param["optim"] == "adadelta":
        optimizer = optim.Adadelta(
            net.parameters(), lr=learning_rate, weight_decay=weight_decay
        )
        print param["optim"]

    # Learning rate scheduler
    #scheduler = optim.lr_scheduler.StepLR(
    #    optimizer, step_size=lr_step_size, gamma=lr_gamma
    #)

    scheduler = None

    # ID of the run
    experiment_ID = (
        "%s_%s_%s_bs(%d)lr(%.3f)e(%d)act(%s)xavier(yes)do(%.1f)N_sinc(%d)sinc_size(%d)depth(%d)"
        % (
            type(net).__name__,
            type(criterion).__name__,
            type(optimizer).__name__,
            batch_size,
            learning_rate,
            max_epochs,
            type(act).__name__,
            param["dropout"],
            param["sinc_filters"],
            param["sinc_size"],
            param["conv_depth"],
        )
    )

    # Logger to save experiment statistics and progress
    log = Logger(
        os.path.join("../", type(net).__name__, type(data["train"]).__name__),
        experiment_ID,
    )

    log.print_parameters(param)

    # Create a DataLoader for each partition of the dataset
    dataloader_train = torch.utils.data.DataLoader(
        data["train"],
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    dataloader_validation = torch.utils.data.DataLoader(
        data["valid"],
        batch_size=len(data["valid"]),
        num_workers=num_workers,
        pin_memory=True,
    )
    dataloader_test = torch.utils.data.DataLoader(
        data["test"],
        batch_size=len(data["test"]) / 4,
        num_workers=num_workers,
        pin_memory=True,
    )

    # Lists used to keep track of the statistics
    losses = []
    train_accuracies = []
    valid_accuracies = []
    ticks = []

        # TRAINING PHASE
    for epoch in range(1, max_epochs + 1):
        t0 = time.time()  # Measure computational time to complete one epoch
        avg_loss, accuracy_train = models.train(
            net, dataloader_train, scheduler, criterion, optimizer
        )
        # Verify the training progress on the validation set
        predictions, _ = models.test(net, dataloader_validation)
        accuracy_validation = (
            100
            * predictions.eq(torch.LongTensor(data["valid"].get_labels()))
            .sum()
            .float()
            / len(data["valid"])
        )
        # Save statistics
        losses.append(avg_loss)
        train_accuracies.append(accuracy_train)
        valid_accuracies.append(accuracy_validation)
        ticks.append(epoch)
        t1 = time.time()
        # Write current statistics on disk
        log.update_train(
            epoch, avg_loss, accuracy_train, accuracy_validation, (t1 - t0)
        )
        # Save best model
        if (epoch - 1) == np.argmax(valid_accuracies):
            torch.save(
                {"net": net, "accuracy": max(valid_accuracies), "epoch": epoch},
                os.path.join(
                    ".",
                    log.experiment_folder,
                    experiment_ID + "FOLD_" + str(i + 1) + ".tar",
                ),
            )
    log.stats_plot(ticks, train_accuracies, valid_accuracies, losses)
    # TEST PHASE
    # Load best model achieved during the training phase
    best_model = torch.load(
        os.path.join(
            ".",
            log.experiment_folder,
            experiment_ID + "FOLD_" + str(i + 1) + ".tar",
        )
    )
    net = best_model["net"]
    if torch.cuda.is_available():
        # Network to GPU
        net.to(device)
    predictions, scores = models.test(net, dataloader_test)
    test_accuracy = (
        100.0
        * predictions.eq(torch.from_numpy(np.asarray(data["test"].get_labels())))
        .sum()
        .float()
        / len(data["test"])
    )
    # Save accuracy on test set
    log.test_stats(best_model["epoch"], best_model["accuracy"], test_accuracy)
    # Compute and save ROC curve and AUC for each class
    roc = ROC(data["N"])
    if scores.is_cuda:
        scores = scores.cpu()
    roc.compute_ROC(data["test"].get_labels(), scores)
    roc.save_all(os.path.join(log.experiment_folder, "ROC"))
    log.auc_roc(roc.roc_auc)
    """
    fold_auc[(i + 1)] = roc.roc_auc.values()
        fold_valid_acc[(i + 1)] = best_model["accuracy"]
        fold_test_acc[(i + 1)] = test_accuracy

        # RESET
        net.__init__(net_param)
        optimizer.__init__(
            net.parameters(),
            lr=learning_rate,
            amsgrad=True,
            weight_decay=param["weight_decay"],
        )
        scheduler.__init__(optimizer, step_size=lr_step_size, gamma=lr_gamma)

        losses = []
        valid_accuracies = []
        train_accuracies = []
        ticks = []

        if k_fold > 1:

            print "next train"
            data["train"].next_fold()
            print "next valid"
            data["valid"].next_fold()
            print "next test"
            data["test"].next_fold()
    """
    if hasattr(net, "viewer"):
        net.viewer.path = os.path.join(
            log.experiment_folder, "FOLD_" + str(i + 1) + "_SINC"
        )
        net.apply(net.viewer)

    #log.fold_stats(fold_valid_acc, fold_test_acc, fold_auc)


if __name__ == "__main__":
    main()
