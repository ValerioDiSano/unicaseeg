#!/usr/bin/env python2

import time
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms

from BCI_data.competition_III_1 import competition_III_1
from BCI_data.competition_III_3a import competition_III_3a
from BCI_data.competition_III_4a import competition_III_4a
from BCI_data.competition_IV import competition_IV

from BCI_data.transforms import Convert
from BCI_data.transforms import Label_decrement
from BCI_data.transforms import LabelMap

from models.myNet import SincEEG
from models.EEGnet import EEGnet
from models.wClipper import MaxNorm
from models.sincLayer import SincViewer
from models.trainNet import train
from models.trainNet import test

from utils.roc import ROC
from utils.netUtils import activation_layer, device_id
from utils.dataOp import compute_balance_weights
from utils.logger import Logger
from utils.others import pars_parameters
from utils.dictionaries import merge_dicts


import os
import datetime



def main():

    param = pars_parameters()  # Retrieve command line parameters

    #k_fold = param["k_fold"]
    singleCase = param["case_name"]
    
    if len(singleCase):
        print singleCase

    sampleTransformT = torchvision.transforms.Compose(
        [
            Convert()
        ]
    )

    # Dataset Selection
    if param["dataset"] == "compIII":

        data = competition_III_1(
            preprocessing=param["prep"],
            labelTransform=LabelMap(),
            sampleTransformTrain=sampleTransformT,
            sampleTransform=Convert(),
            augumentationFunc=None,
        )
    elif param["dataset"] == "compIV":
        data = competition_IV(
            sample_transform=Convert(),
            label_transform=Label_decrement(),
            preprocessing=param["prep"],
            subject=singleCase
        )
    elif param["dataset"] == "compIII4a":
        data = competition_III_4a(
            sampleTransform=Convert(),
            labelTransform=Label_decrement(),
            preprocessing=param["prep"],
            selectedCase=singleCase,
            newRate=128,
        )
    elif param["dataset"] == "compIII3a":
        data = competition_III_3a(
            sampleTransform=Convert(),
            labelTransform=Label_decrement(),
            preprocessing=param["prep"],
            selectedCase=singleCase,
        )

    # Network Hyperparameters
    batch_size = param["batch_size"]
    learning_rate = param["learning_rate"]
    max_epochs = param["epochs"]

    #lr_step_size = param["step_size"]
    #lr_gamma = param["gamma"]

    # Network's layers parameters
    act = activation_layer(param["activation"])
    # Max norm regularization
    clip = MaxNorm()

    # class delegated to print the sync
    viewer = SincViewer()

    # Options GPU / number of CPU threads to load the data
    device = device_id(param["device"])
    torch.cuda.set_device(param["device"])
    num_workers = 16

    # Get all paramters
    net_param = merge_dicts(
        {
            k: param[k]
            for k in ("dropout", "sinc_filters", "sinc_size", "depth", "pool_size")
        },
        {k: data[k] for k in ("fs", "C", "T", "N")},
        {"act": act, "clipper": clip, "viewer": viewer},
    )

    # Create network
    net = SincEEG(net_param)
    # net = models.SincConvNet( net_param )
    # net = models.myResNet( net_param )
    # net = models.MobileSinc( net_param )
    #net = EEGnet(8, net_param["C"], net_param["depth"], net_param["T"], net_param["N"], act, net_param["dropout"], clipper=clip)

    if torch.cuda.is_available():
        # Network to GPU
        net.to(device)

    ### Loss function ###
    if param["weights"]:
        w = torch.FloatTensor(
            compute_balance_weights(data["train"].get_labels()).values()
        )
        if torch.cuda.is_available():
            w = w.cuda()
    else:
        w = None

    criterion = nn.CrossEntropyLoss()
    # criterion = FocalLoss( gamma=0.5 )
    # criterion = nn.MultiMarginLoss()
    # criterion = nn.BCELoss( )

    ### Optimizer ###
    if param["optim"] == "adam":
        optimizer = optim.Adam(
            net.parameters(),
            lr=learning_rate,
            amsgrad=False,
            weight_decay=param.get("w_decay", None),
        )
    elif param["optim"] == "rmsprop":
        optimizer = optim.RMSprop(
            net.parameters(), lr=learning_rate, weight_decay=param.get("w_decay", None)
        )
    elif param["optim"] == "adadelta":
        optimizer = optim.Adadelta(
            net.parameters(), lr=learning_rate, weight_decay=param.get("w_decay", None)
        )
        print param["optim"]

    # Learning rate scheduler
    # scheduler = optim.lr_scheduler.StepLR( optimizer, step_size  = lr_step_size, gamma=lr_gamma )
    scheduler = None

    # ID of the run
    experiment_ID = (
        "%s_%s_%s_bs(%d)lr(%.3f)e(%d)act(%s)xavier(yes)do(%.1f)N_sinc(%d)sinc_size(%d)depth(%d)"
        % (
            type(net).__name__,
            type(criterion).__name__,
            type(optimizer).__name__,
            batch_size,
            learning_rate,
            max_epochs,
            type(act).__name__,
            param["dropout"],
            param["sinc_filters"],
            param["sinc_size"],
            param["depth"],
        )
    )

    # Logger to save experiment statistics and progress
    log = Logger(
        os.path.join("../", type(net).__name__, (data["train"]).__name__), experiment_ID
    )

    log.print_parameters(param)

    # Create a DataLoader for each partition of the dataset
    dataloader_train = torch.utils.data.DataLoader(
        data["train"],
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=True,
    )
    dataloader_validation = torch.utils.data.DataLoader(
        data["valid"],
        batch_size=len(data["valid"]),
        num_workers=num_workers,
        pin_memory=True,
    )
    dataloader_test = torch.utils.data.DataLoader(
        data["test"],
        batch_size=len(data["test"]) / 4,
        num_workers=num_workers,
        pin_memory=True,
    )

    # Lists used to keep track of the statistics
    losses = []
    train_accuracies = []
    valid_accuracies = []
    ticks = []

    fold_auc = {}
    fold_valid_acc = {}
    fold_test_acc = {}

    gap = 101.0
    bestLoss = 100
    bestValidAcc = 0.0


    # TRAINING PHASE
    for epoch in range(1, max_epochs + 1):
        t0 = time.time()  # Measure computational time to complete one epoch
        avg_loss, accuracy_train = train(
            net, dataloader_train, scheduler, criterion, optimizer
        )
        # Verify the training progress on the validation set
        print "Validation"
        predictions, _ = test(net, dataloader_validation)
        # predictions = torch.LongTensor( BCI_data.majorityVoting( predictions ) )
        # accuracy_validation = 100 * predictions.eq(torch.LongTensor( BCI_data.majorityVoting(data["valid"].get_labels()) )).sum().float() / len( predictions )
        accuracy_validation = (
            100
            * predictions.eq(torch.LongTensor((data["valid"].get_labels())))
            .sum()
            .float()
            / len(data["valid"])
        )
        # Save statistics
        losses.append(avg_loss)
        train_accuracies.append(accuracy_train)
        valid_accuracies.append(accuracy_validation)
        ticks.append(epoch)
        t1 = time.time()
        # Write current statistics on disk
        log.update_train(
            epoch, avg_loss, accuracy_train, accuracy_validation, (t1 - t0)
        )
        newGap = abs(accuracy_validation - accuracy_train)
        # Save best model
        # if ( epoch - 1 ) == np.argmax( valid_accuracies ) and ( newGap <= gap ):
        # if (accuracy_validation > bestValidAcc) or (accuracy_validation == bestValidAcc and newGap <= gap):
        if (accuracy_validation > bestValidAcc) or (
            accuracy_validation == bestValidAcc and losses[-1] <= bestLoss
        ):
            gap = newGap
            bestValidAcc = accuracy_validation
            bestLoss = losses[-1]
            bestEpoch = epoch
            torch.save(
                {"net": net, "accuracy": max(valid_accuracies), "epoch": epoch},
                os.path.join(
                    ".",
                    log.experiment_folder,
                    experiment_ID + ".tar",
                ),
            )
            # torch.save(net.state_dict(),os.path.join(".",log.experiment_folder,experiment_ID + "FOLD_" + str(i+1) + "bestNet.pt"))
    log.stats_plot(ticks, train_accuracies, valid_accuracies, losses)
    # TEST PHASE
    # Load best model achieved during the training phase
    best_model = torch.load(
        os.path.join(
            ".",
            log.experiment_folder,
            experiment_ID + ".tar",
        )
    )
    
    net = best_model["net"]
    net.eval()

    if torch.cuda.is_available():
        # Network to GPU
        net.to(device)
    
    t0 = time.time()
    predictions, scores = test(net, dataloader_test)
    t1 = time.time()

    test_accuracy = (
        100
        * predictions.eq(torch.LongTensor(data["test"].get_labels())).sum().float()
        / len(data["test"])
    )
  
    # Save accuracy on test set
    log.test_stats(best_model["epoch"], best_model["accuracy"], test_accuracy, (t1-t0))
    # Compute and save ROC curve and AUC for each class
    roc = ROC(data["N"])
    if scores.is_cuda:
        scores = scores.cpu()

    roc.compute_ROC(data["test"].get_labels(), scores)
    roc.save_all(os.path.join(log.experiment_folder, "ROC"))
    log.auc_roc(roc.roc_auc)


    
    #fold_auc[(i + 1)] = roc.roc_auc.values()
    #fold_valid_acc[(i + 1)] = bestValidAcc
    #fold_test_acc[(i + 1)] = test_accuracy

    """
    # RESET
    net.__init__(net_param)
    optimizer.__init__(
        net.parameters(),
        lr=learning_rate,
        amsgrad=True,
        weight_decay=param["w_decay"],
    )
    scheduler.__init__(optimizer, step_size=lr_step_size, gamma=lr_gamma)
    losses = []
    valid_accuracies = []
    train_accuracies = []
    ticks = []
    """
    """
    if k_fold > 1:
        print "next train"
            data["train"].next_fold()
            print "next valid"
            data["valid"].next_fold()
            print "next test"
            data["test"].next_fold()
    """
    if hasattr(net, "viewer"):
        net.viewer.path = os.path.join(
            log.experiment_folder, "SINC"
        )
        net.apply(net.viewer)

    #log.fold_stats(fold_valid_acc, fold_test_acc, fold_auc)


if __name__ == "__main__":
    main()
