# How to use

Eseguire lo script train_test_SincEEG.py fornendo i parametri tramite un file json:


```bash
./train_test_SincEEG parameters_SincEEG.json
```

Il file [.json] deve contenere le seguenti informazioni:

- batch_size
- learning_rate
- epochs 	--> numero di epoche su cui eseguire il training
- activation 	--> activation function
- dropout
- sinc_filters	--> numero di filtri sinc da usare
- sinc_size 	--> numero di campioni utilizzati per ogni segnale sinc
- depth 	--> fattore di depth nella depth wise convolution
- device 	--> numero della GPU da utilizzare
- weights	--> boolean per indicare se utilizzare una loss pesata
- prep		--> string contentente la sequenza di preprocessing da applicare con ogni stage separato dal carattere ':'. Per i metodi di preprocessing disponibile vedere il file BCI_data/preprocessing.py
- optim 	--> optimizer
- pool_size 	--> dimensione del filtro di pooling
- w_decay 	--> fattore di weight decay (= 0 per non utilizzarlo)
- case_name	--> stringa contenente l'id del paziente. Una stringa vuota equivale a caricare i dati di tutti i pazienti.


## Repository structure

- BCI_data/ => Funzioni necessarie a caricare i dataset e a processare i dati

- models/ => Implementazione delle reti e del layer convoluzionale basato su sinc            

- signals/ => Segnale sinc (1D/2D) differenziabile tramite AutoGrad

- utils/ => Funzioni di utilità

## DATA

Di default si suppone che i dati siano inseriti nel path ./datasets/
