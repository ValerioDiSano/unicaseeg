#!/usr/bin/env python2
import sys
import json

def main(argv):

    json_path = argv[1]
    subject = argv[2]

    print "JSON file: {}".format(json_path)
    print "Subject: {}".format(subject)

    with open(json_path, "r") as json_file:
        data = json.load(json_file)

        data["case_name"] = subject

    with open(json_path, "w") as json_file:
        data = json.dumps(data, indent=4)
        json_file.write(data)

if __name__ == "__main__":
    main(sys.argv)