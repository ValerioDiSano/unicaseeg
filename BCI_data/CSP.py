import numpy as np
import scipy
import utils


class CSP:
    def __init__(self, X, y, N):
        self.W = self.compute_W(X, y, int(N))

    def compute_W(self, X, y, N):

        sampleSize = X[X.keys()[0]].shape
        C = sampleSize[0]
        classCount = np.zeros(N)

        # Covariance matrix for each class
        Rc = np.zeros([N, C, C])
        # Sum of covariance matrices
        R = np.zeros((C, C))

        for key, sample in X.items():

            # print np.sum( np.isinf(sample) )
            # print ""
            sample = np.real(sample)
            Cov = np.matmul(sample, np.transpose(sample))
            Cov /= np.trace(Cov)

            classIdx = int(y[key] - 1)

            Rc[classIdx, :, :] += Cov
            classCount[classIdx] += 1

        print classCount
        # Sum of covariance matrices
        for i in range(N):
            Rc[i, :, :] /= classCount[i]
            np.nan_to_num(Rc[i, :, :], copy=False)
            R += Rc[i, :, :]

        Sig, U0 = np.linalg.eig(R)
        Sig = np.diag(Sig)  # Diagonal form of the eigenvalues

        P = np.matmul(scipy.linalg.fractional_matrix_power(Sig, -0.5), np.transpose(U0))

        S0 = np.matmul(P, Rc[0, :, :])
        S0 = np.matmul(S0, np.transpose(P))

        _, U = np.linalg.eig(S0)

        self.W = np.matmul(np.transpose(U), P)
        return self.W

    def transformData(self, X):

        transform = lambda x: np.matmul(self.W, x)
        X = utils.dict_MT(X, transform)

        return X
