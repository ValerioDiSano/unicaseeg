import mat4py
import os, sys
import scipy.io
import math
import numpy as np

from BCI_III_inMem import BCI_III_inMem
import numpy as np
from BCI_inMem import getOutputDict 
from preprocessing import preprocessingSets
from utils.dataOp import random_subdictionary
from utils.dataOp import load_labels

def competition_III_1(
    root_dir="./datasets/bci_comp_I",
    new_fs=128,
    preprocessing="FIR:power:normpersample:mean_std:resample",
    validPercentage=10,
    sampleTransformTrain=None,
    sampleTransform=None,
    labelTransform=None,
    randomValid=True,
    save=True,
    augumentationFunc=None,
):
    ext = ".mat"  # Dataset file exstension
    N = 2  # Number of classes
    sample_rate = 1e3  # Original sample rate

    trainPath = os.path.join(root_dir, "Competition_train" + ext)
    testPath = os.path.join(root_dir, "Competition_test" + ext)
    testLabelsPath = os.path.join(root_dir, "true_labels.txt")

    yTest = load_labels(testLabelsPath)
    print yTest
    yTest = dict(enumerate(yTest))

    back = {
        "train": None,  # Training dataset
        "valid": None,  # Validation dataset
        "test": None,  # Testing set
        "N": N,  # Number of classes
        "T": None,  # Time points per sample
        "C": None,  # Number of channels
        "fs": sample_rate,  # frequency of sampling
    }

    trainData = scipy.io.loadmat(trainPath)
    XTrain = np.array(trainData["X"])
    XTrain = np.asfarray(XTrain, dtype="float")

    back["C"] = XTrain.shape[1]
    back["T"] = XTrain.shape[2]

    XTest = scipy.io.loadmat(testPath)
    XTest = np.array(XTest["X"])
    XTest = np.asfarray(XTest, dtype="float")

    XTrain = dict(enumerate(XTrain))
    yTrain = np.array(trainData["Y"])
    yTrain = dict(enumerate(yTrain))

    XTest = dict(enumerate(XTest))

    validLen = int(len(XTrain) * validPercentage / 100.0)

    print validLen

    if randomValid:

        XValid, XTrain = random_subdictionary(XTrain, validLen)

        yValid = {}

        for key in XValid.keys():
            yValid[key] = yTrain.pop(key)

        # Remapping

        for i, key in enumerate(sorted(XTrain.keys())):
            XTrain[i] = XTrain.pop(key)
            yTrain[i] = yTrain.pop(key)

        for i, key in enumerate(sorted(XValid.keys())):
            XValid[i] = XValid.pop(key)
            yValid[i] = yValid.pop(key)
    else:
        XValid = {}
        yValid = {}
        trainSize = len(XTrain)
        for i in range(validLen):
            XValid[i] = XTrain.pop(trainSize - i - 1)
            yValid[i] = yTrain.pop(trainSize - i - 1)

    print len(XValid)

    if save:
        savePath = root_dir
    else:
        savePath = None

    XTrain, XTest, XValid = preprocessingSets(
        XTrain,
        XTest,
        XValid,
        preprocessing,
        new_fs,
        sample_rate,
        savePath=savePath,
        yTrain=yTrain,
    )

    back["C"] = XTrain[0].shape[0]
    back["T"] = XTrain[0].shape[1]
    # back["T"] = 96
    if new_fs:
        back["fs"] = new_fs

    back["train"] = BCI_III_inMem(
        XTrain,
        yTrain,
        sampleTransform=sampleTransformTrain,
        labelTransform=labelTransform,
        name="BCI_III_1",
        augumentationFunc=augumentationFunc,
    )
    back["valid"] = BCI_III_inMem(
        XValid,
        yValid,
        sampleTransform=sampleTransform,
        labelTransform=labelTransform,
        name="BCI_III_1",
        augumentationFunc=augumentationFunc,
    )
    back["test"] = BCI_III_inMem(
        XTest,
        yTest,
        sampleTransform=sampleTransform,
        labelTransform=labelTransform,
        name="BCI_III_1",
        augumentationFunc=augumentationFunc,
    )

    return back
