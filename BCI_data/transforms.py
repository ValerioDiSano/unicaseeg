import torch
import random
import numpy as np
from scipy.ndimage.interpolation import shift


class Convert(object):
    """ Convert class helps to convert python iterable object into torch float tensor
    """

    def __call__(self, sample):
        """ return a flaot torch tensor of size 1 x C x T
        """
        return torch.unsqueeze(torch.from_numpy(np.real(sample)), 0).float()


class Delay(object):
    def __call__(self, sample):

        T = sample.shape[1]
        for row in range(sample.shape[0]):
            tau = random.randint(-int(T / 2), int(T / 2))
            tau = random.randint(0, int(T / 2))
            sample[row, :] = shift(sample[row, :], tau)

        return sample


class CropRandom(object):
    def __init__(self, cropPerc):

        self.cropPerc = cropPerc
        self.T = None
        self.gen = None

    def __call__(self, sample):

        if not self.T:
            self.T = int((self.cropPerc * sample.shape[1]) / 2)
            self.gen = lambda: random.randint(self.T, sample.shape[1] - self.T)

        t = self.gen()

        return sample[:, t - self.T : t + self.T]


class ShuffleChannels(object):
    def __call__(self, sample):
        np.random.shuffle(sample)
        return sample


class Convert_1D(object):
    """
    """

    def __call__(self, sample):
        return torch.from_numpy(sample).float()


class Label_decrement(object):
    def __call__(self, label):
        return int(label - 1)


class LabelMap(object):
    def __call__(self, label):

        return 1 if (label == 1) else 0

