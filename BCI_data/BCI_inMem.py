import torch.utils.data as data
import numpy as np


class BCI_inMem(data.Dataset):
    def __init__(
        self, gen_func, start_idx, length, transform=None, label_transform=None
    ):
        self.gen_func = gen_func
        self.start_idx = start_idx
        self.size = length

        self.transform = transform
        self.label_transform = label_transform


    def __len__(self):
        return self.size

    def __getitem__(self, idx):
        sample = self.gen_func.X[(self.start_idx + idx) % self.gen_func.n_samples, :, :]
        label = (
            1
            if self.gen_func.y[(self.start_idx + idx) % self.gen_func.n_samples] == 1
            else 0
        )

        if self.transform:
            sample = self.transform(sample)

        if self.label_transform:
            label = self.label_transform(label)
        return sample, label

    def get_labels(self):

        end_idx = self.start_idx + self.size
        if end_idx > self.gen_func.n_samples:
            end_idx = end_idx % self.gen_func.n_samples
            labels = self.gen_func.y[self.start_idx :] + self.gen_func.y[:end_idx]
        else:
            labels = self.gen_func.y[self.start_idx : end_idx]

        return [1 if x > 0 else 0 for x in labels]

    def next_fold(self):
        print ""
        self.start_idx = (
            self.start_idx + self.gen_func.k_fold_len
        ) % self.gen_func.n_samples
        print self.start_idx
        end_idx = self.start_idx + self.size
        if end_idx > self.gen_func.n_samples:
            end_idx = end_idx % self.gen_func.n_samples
        print end_idx
        print ""


class BCIinMem(data.Dataset):
    def __init__(self, X, y, fs, sample_transform, label_transform, name, sets=None, subjects = None
    ):
        self.X = X
        self.y = y
        self.fs = fs
        if sets:
            self.sets = sets
        self.sample_transform = sample_transform
        self.label_transform = label_transform

        self.size = len(y)

        if name:
            print name
            self.__name__ = name

        self.subjects = subjects    # Dictionary.
                                    # The key is the subject name,
                                    # the value is the interval in which the subject samples can be found.

    def __len__(self):
        return self.size

    def __getitem__(self, idx):

        sample = self.X[idx]
        label = self.y[idx]

        if self.sample_transform:
            sample = self.sample_transform(sample)

        if self.label_transform:
            label = self.label_transform(label)

        return sample, label

    def get_labels(self):

        return [self.y[k] - 1 for k in sorted(self.y.keys())]


def get_samples(X, t, fs, C, y, interval=(0, 2), subset=False):

    samples = []
    idx = []
    interval = np.array(interval)

    interval *= fs

    for i in range(len(t)):

        # print t[i]
        # print t.shape

        if subset and y[i] > 2:
            continue
        
        samples.append(X[:C, int(t[i] + interval[0]) : int(t[i] + interval[1])])
        idx.append(i)

    return samples, idx



def getOutputDict(
    trainDict=None,
    validDict=None,
    testDict=None,
    N=None,
    T=None,
    C=None,
    sampleRate=None,
):

    back = {
        "train": trainDict,  # Training dataset
        "valid": validDict,  # Validation dataset
        "test": testDict,  # Testing set
        "N": N,  # Number of classes
        "T": T,  # Time points per sample
        "C": C,  # Number of channels
        "fs": sampleRate,  # frequency of sampling
    }

    return back