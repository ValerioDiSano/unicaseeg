import torch.utils.data as data

class BCI_III_inMem(data.Dataset):
    def __init__(
        self,
        X,
        y,
        sampleTransform=None,
        labelTransform=None,
        name="BCI_III_1",
        augumentationFunc=None,
    ):

        if augumentationFunc:
            self.X, self.y = augumentationFunc(X, y)
        else:
            self.X = X
            self.y = y

        self.sampleTrasnform = sampleTransform
        self.labelTransform = labelTransform

        self.__name__ = name

    def __len__(self):

        return len(self.y)

    def __getitem__(self, idx):

        sample = self.X[idx]
        label = self.y[idx]

        if self.sampleTrasnform:
            sample = self.sampleTrasnform(sample)
        if self.labelTransform:
            label = self.labelTransform(label)

        return sample, label

    def get_labels(self):

        # return [ 1 if self.y[key] > 0  else 0 for key in sorted( self.y.keys() ) ]
        return [self.labelTransform(self.y[key]) for key in sorted(self.y.keys())]

