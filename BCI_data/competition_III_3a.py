from BCI_inMem import BCIinMem
import numpy as np
from BCI_inMem import getOutputDict 
from preprocessing import preprocessingSets
from utils.others import recursiveVisit
from utils.dataOp import random_subdictionary

import scipy.io
import mat4py


def competition_III_3a(
    rootDir=["./datasets/BCI_comp_III_3a"],
    timeInterval=(3.0, 7.0),
    validPerc=10,
    sampleTransform=None,
    labelTransform=None,
    preprocessing="FIR:resample:primeDiff",
    save=True,
    newRate=128,
    selectedCase="",
):

    print sampleTransform
    print labelTransform

    ext = ".mat"
    labelExt = ".txt"
    # pExt = ".edf"
    N = 4  # LEFT Hand / RIGHT Hand / TONGUE / FOOT
    C = 60
    sampleRate = 250  # [HZ]

    print selectedCase

    # if len(selectedCase):
    #    selectedCase = selectedCase.split(":")

    back = getOutputDict(sampleRate=sampleRate, N=N, C=C)

    matFiles = recursiveVisit(rootDir, (ext, labelExt), verbose=False)

    testLabelsPath = []
    # edfPath = []

    j = 0
    for k in range(len(matFiles)):
        # print matFiles[j]
        if "label" in matFiles[j]:
            testLabelsPath.append(matFiles.pop(j))
        elif "processed" in matFiles[j]:
            matFiles.pop(j)
        # elif pExt in matFiles[j]:
        #    edfPath.append( matFiles.pop(j) )
        else:
            j += 1
    # print testLabelsPath
    # print matFiles

    trainCount = 0
    testCount = 0
    XTrain = {}
    XTest = {}
    yTrain = {}
    yTest = {}

    for samplePath in matFiles:

        print samplePath

        caseName = samplePath[-len(ext) - 3 : -len(ext) - 1]
        print caseName
        print testLabelsPath

        if len(selectedCase) and caseName not in selectedCase.split(":"):
            continue

        data = scipy.io.loadmat(samplePath)
        caseLabels = [name for name in testLabelsPath if caseName in name]
        caseLabels = caseLabels.pop()

        # edfFile = [ name for name in edfPath if caseName in name ]
        # print edfPath
        # edfFile = edfFile.pop()
        # edfFile = pyedflib.EdfReader( edfFile )

        caseLabels = dict(enumerate(utils.load_labels(caseLabels)))

        X = np.transpose(data["s"]).astype(float)
        # X = np.zeros((C,edfFile.readSignal(0).shape[0]))
        # for i in range(C):

        #    X[i,:] = edfFile.readSignal(i)

        # print X.shape

        np.nan_to_num(X, copy=False)
        # X = np.multiply(X,0.1)
        y = data["HDR"]["Classlabel"][0]
        y = y[0]

        y = np.array([y.item(i) for i in range(len(y))])
        idx = data["HDR"]["TRIG"][0]
        idx = idx[0]

        X, _ = get_samples(X, idx, sampleRate, C, y, timeInterval)

        for k, sample in enumerate(X):
            if not utils.isNan(y[k]):
                XTrain[trainCount] = sample
                yTrain[trainCount] = y[k]
                # print yTrain[trainCount]
                trainCount += 1
            else:
                XTest[testCount] = sample
                yTest[testCount] = caseLabels[k]
                # print yTest[testCount]
                testCount += 1

    back["T"] = int(back["fs"] * (timeInterval[1] - timeInterval[0]))
    print "N time samples: %d" % (back["T"],)
    print back["fs"]

    validSize = int(trainCount * (validPerc / 100.0))

    XValid, _ = random_subdictionary(XTrain, validSize)
    yValid = {}

    for key in XValid.keys():
        yValid[key] = yTrain.pop(key)

    # Remapping

    for i, key in enumerate(sorted(XTrain.keys())):
        XTrain[i] = XTrain.pop(key)
        yTrain[i] = yTrain.pop(key)

    for i, key in enumerate(sorted(XValid.keys())):
        XValid[i] = XValid.pop(key)
        yValid[i] = yValid.pop(key)

    if save:
        savePath = rootDir[0]
    else:
        savePath = None

    XTrain, XTest, XValid = preprocessingSets(
        XTrain, XTest, XValid, preprocessing, newRate, sampleRate, savePath=savePath
    )

    back["C"] = XTrain[0].shape[0]
    back["T"] = XTrain[0].shape[1]
    if newRate:
        back["fs"] = newRate

    back["train"] = BCIinMem(
        XTrain,
        yTrain,
        back["fs"],
        sampleTransform,
        labelTransform,
        name="BCI_III_3a_" + selectedCase,
    )
    back["valid"] = BCIinMem(
        XValid,
        yValid,
        back["fs"],
        sampleTransform,
        labelTransform,
        name="BCI_III_3a_" + selectedCase,
    )
    back["test"] = BCIinMem(
        XTest,
        yTest,
        back["fs"],
        sampleTransform,
        labelTransform,
        name="BCI_III_3a_" + selectedCase,
    )

    return back

