from utils.preprocessing import dict_normDataset
from utils.preprocessing import dict_ZCA
from utils.preprocessing import dict_FIR
from utils.preprocessing import dict_resample
from utils.preprocessing import dict_FirResample
from utils.preprocessing import dict_normPerSample
from utils.preprocessing import dict_ICA
from utils.preprocessing import dict_PowerTransform
from utils.preprocessing import dict_PrimeDifference
from utils.preprocessing import dict_RobustScale
from utils.preprocessing import dict_MinMaxScale
from utils.dictionaries import saveDataDict

from BCI_data.CSP import CSP

import os



def preprocessingSets(
    XTrain, XTest, XValid, preprocessing, newRate, oldRate, savePath=None, yTrain=None
):
    for prep in preprocessing.split(":"):
        if prep == "mean_std":

            XTrain, Cov, mean = utils.dict_normDataset(XTrain)
            XTest, _, _ = dict_normDataset(XTest, mean=mean, std=Cov)
            XValid, _, _ =dict_normDataset(XValid, mean=mean, std=Cov)
        elif prep == "ZCA":
            XTrain, T = dict_ZCA(XTrain, T=None, PCA=False)
            XTest, _ = dict_ZCA(XTest, T=T, PCA=False)
            XValid, _ = dict_ZCA(XValid, T=T, PCA=False)
        elif prep.lower() == "csp":
            N = max(yTrain.values())
            Csp = CSP(XTrain, yTrain, N)
            XValid = Csp.transformData(XValid)
            XTrain = Csp.transformData(XTrain)
            XTest = Csp.transformData(XTest)

        elif prep == "FIR":
            XTest = dict_FIR(XTest, oldRate, highF=newRate / 2, lowF=0.0)
            XTrain = dict_FIR(XTrain, oldRate, highF=newRate / 2, lowF=0.0)
            XValid = dict_FIR(XValid, oldRate, highF=newRate / 2, lowF=0.0)

        elif prep.lower() == "resample":

            XTest = dict_resample(XTest, newRate, oldRate)
            XTrain = dict_resample(XTrain, newRate, oldRate)
            XValid = dict_resample(XValid, newRate, oldRate)

        elif prep.lower() == "firresample":
            XTest = dict_FirResample(XTest, newRate, oldRate)
            XTrain = dict_FirResample(XTrain, newRate, oldRate)
            XValid = dict_FirResample(XValid, newRate, oldRate)

        elif prep.lower() == "normpersample":
            #print "Norm Per sample"

            # XTest = utils.dict_MeanStdPerSample( XTest )
            # XTrain = utils.dict_MeanStdPerSample( XTrain )
            # XValid = utils.dict_MeanStdPerSample( XValid )

            XTest = dict_normPerSample(XTest)
            XTrain = dict_normPerSample(XTrain)
            XValid = dict_normPerSample(XValid)

        elif prep.lower() == "ica":

            XValid = dict_ICA(XValid)
            XTrain = dict_ICA(XTrain)
            XTest = dict_ICA(XTest)

        elif prep.lower() == "power":

            XValid = dict_PowerTransform(XValid)
            XTrain = dict_PowerTransform(XTrain)
            XTest = dict_PowerTransform(XTest)

        elif prep.lower() == "primediff":

            XValid = dict_PrimeDifference(XValid)
            XTrain = dict_PrimeDifference(XTrain)
            XTest = dict_PrimeDifference(XTest)

        elif prep.lower() == "robustscale":

            XValid = dict_RobustScale(XValid)
            XTrain = dict_RobustScale(XTrain)
            XTest = dict_RobustScale(XTest)

        elif prep.lower() == "minmax":

            XValid = dict_MinMaxScale(XValid)
            XTrain = dict_MinMaxScale(XTrain)
            XTest = dict_MinMaxScale(XTest)

    if savePath:
        saveDataDict(XTrain, path=os.path.join(savePath, "processed", "Train"))
        saveDataDict(XValid, path=os.path.join(savePath, "processed", "Valid"))
        saveDataDict(XTest, path=os.path.join(savePath, "processed", "Test"))

    return XTrain, XTest, XValid


def augumentationData(X, y, nSubSamples=4):

    T = X.values()[0].shape[1]
    t = int(T / nSubSamples)

    XAug = {}
    yAug = {}

    for i in range(len(X)):

        for j in range(nSubSamples):

            XAug[i + j] = X[i][:, j * t : (j + 1) * t]
            print XAug[i + j].shape
            yAug[i + j] = y[i]

    return XAug, yAug


def majorityVoting(labels, group=4):

    groupedLabels = []
    if isinstance(labels, torch.Tensor):
        labels = labels.tolist()

    for i in range(len(labels) / group):

        l = labels[group * i : group * (i + 1)]
        d = dict((x, l.count(x)) for x in set(l))
        label = max(d, key=d.get)
        groupedLabels.append(label)

    return groupedLabels
