from BCI_inMem import getOutputDict 
import os
from utils.dataOp import samplingOnPatiences
from utils.others import recursiveVisit
from utils.others import isNan
from utils.dictionaries import dictSubset
import scipy.io
import numpy as np



def competition_III_4a(
    rootDir=["./datasets/BCI_comp_III_4a"],
    timeInterval=(0.0, 3.5),
    validPerc=10,
    sampleTransform=None,
    labelTransform=None,
    preprocessing="CSP:normpersample",
    save=True,
    newRate=None,
    selectedCase="",
    superSampling=True,
):

    print sampleTransform
    print labelTransform

    ext = ".mat"
    N = 2  # RIGHT / FOOT
    C = 118
    sampleRate = 1000  # [HZ]

    back = getOutputDict(sampleRate=sampleRate, N=N, C=C)

    matFiles = recursiveVisit(rootDir, ext, verbose=False)

    testLabelsPath = []

    j = 0
    for k in range(len(matFiles)):
        if "label" in matFiles[j]:
            testLabelsPath.append(matFiles.pop(j))
        elif "processed" in matFiles[j]:
            matFiles.pop(j)
        else:
            j += 1
    # print testLabelsPath
    print matFiles

    trainCount = 0
    testCount = 0
    XTrain = {}
    XTest = {}
    yTrain = {}
    yTest = {}

    patiencesSet = {}
    patiencesTestSet = {}

    for samplePath in matFiles:

        print samplePath

        caseName = samplePath[-len(ext) - 2 : -len(ext)]
        print caseName

        if len(selectedCase) and caseName != selectedCase:
            continue

        data = scipy.io.loadmat(samplePath)
        caseLabels = [name for name in testLabelsPath if caseName in name]
        caseLabels = caseLabels.pop()

        caseLabels = scipy.io.loadmat(caseLabels)
        caseLabels = caseLabels["true_y"][0, :]

        X = np.transpose(data["cnt"]).astype(float)
        # X = np.multiply(X,0.1)
        y = data["mrk"][0][0][1]
        y = y[0, :]
        idx = data["mrk"][0][0][0]
        idx = idx[0, :]

        X, _ = get_samples(X, idx, sampleRate, C, y, timeInterval)

        firstSample = trainCount
        firstTestSample = testCount

        for k, sample in enumerate(X):

            if not isNan(y[k]):
                XTrain[trainCount] = sample
                yTrain[trainCount] = y[k]
                trainCount += 1
            else:
                XTest[testCount] = sample
                yTest[testCount] = caseLabels[k]
                testCount += 1

        patiencesSet[caseName] = (firstSample, trainCount - 1)
        patiencesTestSet[caseName] = (firstTestSample, testCount - 1)

    validSize = int(trainCount * (validPerc / 100.0))

    XValid, XTrain, patiencesSet = dictSubset(XTrain, validSize, patiencesSet)
    yValid = {}

    for key in XValid.keys():
        yValid[key] = yTrain.pop(key)

    # Remapping

    for i, key in enumerate(sorted(XTrain.keys())):
        XTrain[i] = XTrain.pop(key)
        yTrain[i] = yTrain.pop(key)

    for i, key in enumerate(sorted(XValid.keys())):
        XValid[i] = XValid.pop(key)
        yValid[i] = yValid.pop(key)

    print "Number of samples before resampling %d" % (len(XTrain))

    if superSampling:
        XTrain, yTrain = samplingOnPatiences(XTrain, yTrain, patiencesSet)
        # XTrain,yTrain = utils.underSampling( XTrain, yTrain, patiencesSet )

    print "Number of samples after resampling %d" % (len(XTrain))

    # back["T"] = int( back["fs"] * ( timeInterval[1] - timeInterval[0] ) )

    # XValid,_ = utils.random_subdictionary( XTrain, validSize )

    if save:
        savePath = rootDir[0]
    else:
        savePath = None

    XTrain, XTest, XValid = preprocessingSets(
        XTrain,
        XTest,
        XValid,
        preprocessing,
        newRate,
        sampleRate,
        savePath=savePath,
        yTrain=yTrain,
    )

    back["C"] = XTrain[0].shape[0]
    back["T"] = XTrain[0].shape[1]
    if newRate:
        back["fs"] = newRate

    back["train"] = BCIinMem(
        XTrain,
        yTrain,
        back["fs"],
        sampleTransform,
        labelTransform,
        name=os.path.join("BCI_III_4a", selectedCase),
    )
    back["valid"] = BCIinMem(
        XValid,
        yValid,
        back["fs"],
        sampleTransform,
        labelTransform,
        name=os.path.join("BCI_III_4a", selectedCase),
    )
    back["test"] = BCIinMem(
        XTest,
        yTest,
        back["fs"],
        sampleTransform,
        labelTransform,
        name=os.path.join("BCI_III_4a", selectedCase),
        sets=patiencesTestSet,
    )

    return back
