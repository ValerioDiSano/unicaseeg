import scipy.io
import os
import numpy as np
from utils.dataOp import random_subdictionary
from preprocessing import preprocessingSets
from BCI_inMem import BCIinMem
from BCI_inMem import get_samples



def competition_IV(
    root_dir="../datasets/bci_comp_IV/",
    time_interval=(0.5, 2.5),
    valid_perc=10,
    sample_transform=None,
    label_transform=None,
    preprocessing="FIR:resample",
    save=True,
    new_rate=128,
    cross_subject=True,
    subject="A06"
):

    if len(subject) == 0:
        raise ValueError("It's necessary to specify a subject to test")

    ext = ".mat"
    N = 4
    C = 25

    # if preprocessing:
    #    sample_rate = 128
    #    old_rate = 250
    # else:
    sample_rate = 250  # [HZ] Original sample rate

    back = {
        "train": None,  # Training dataset
        "valid": None,  # Validation dataset
        "test": None,   # Testing set
        "N": N,         # Number of classes
        "T": None,      # Time points per sample
        "C": C,         # Number of channels
        "fs": sample_rate,  # frequency of sampling
    }

    file_names = [f for f in os.listdir(root_dir) if f.endswith(ext)]

    X_train = {}
    y_train = {}
    X_test = {}
    y_test = {}
    X_valid = {}
    y_valid = {}

    count_train = 0
    count_test = 0
    count_valid = 0

    indices_train = {}
    indices_test = {}
    indices_valid = {}

    for name in file_names:
        sample_name = os.path.join(root_dir, name)  # get the path of the file

        subname = name.split(".")[0]                # take only the file name without the file extension
                                                    # The file name gives information about the subject and the train/test file

        if not cross_subject and subject not in subname:
            # subject contains the name of the patient to consider in the whitin subject or in the cross subject evaluation
            continue

        # Check if the file contains train or test samples
        # Test files end with letter "E"
        if subname.endswith("E"):
            print "test file %s" % (subname,)
            train = False
            # data_test = utils.merge_dicts( scipy.io.loadmat( sample_name ), data_test )
        else:
            print "train file %s" % (subname,)
            train = True
            # data_train = utils.merge_dicts(scipy.io.loadmat(sample_name), data_train )

        mat_file = scipy.io.loadmat(sample_name)
        data = mat_file["data"]
        # print data.shape

        if not train and not (subject in subname):
            continue

        #if not cross_subject and not (subject in subname):
            # for the whitin subject case, if the loaded subject is not the choosen one, go ahead
        #    continue
        
        if train and (subject in subname) and cross_subject:
            continue

        for i in range(max(data.shape)):
            cell = data[0, i]

            cell_dict = {}
            # cell = dict( zip(cell.dtype.names, cell) )
            # print cell.keys()
            for j, key in enumerate(cell.dtype.names):

                cell_dict[key] = cell[(cell.dtype.names[j])]

            # print cell_dict['y'][0][0]
            if not len(cell_dict["y"][0][0]):
                continue

            samples, idx = get_samples(
                np.transpose(cell_dict["X"][0][0]),
                cell_dict["trial"][0][0],
                250,
                C,
                cell_dict["y"][0][0],
                interval=time_interval,
                subset=False,
            )

            print subname
            print len(samples)

            for k, sample in enumerate(samples):
                # print cell_dict['y'][0][0][k]

                # if preprocessing:
                #
                #    sample = utils.FIR_downsampling( sample, old_fs=old_rate,new_fs=sample_rate,axis=1 )
                """
                if cross_subject and train and (subject in subname):
                #validation
                    y_valid[count_valid] = cell_dict["y"][0][0][idx[k]][0]
                    # print y_train[count_train]
                    X_valid[count_valid] = sample
                    count_valid += 1
                    continue
                """


                if train:
                    y_train[count_train] = cell_dict["y"][0][0][idx[k]][0]
                    # print y_train[count_train]
                    X_train[count_train] = sample

                    indices_train.setdefault(subname, []).append(count_train)

                    count_train += 1
                else:
                    y_test[count_test] = cell_dict["y"][0][0][idx[k]][0]
                    # print y_test[count_test]
                    X_test[count_test] = sample
                    count_test += 1

                    indices_test.setdefault(subname, []).append(count_test)

    del cell
    del cell_dict

    # if "resample" in preprocessing.lower():
    #    back["fs"] = 128
    back["T"] = int(back["fs"] * (time_interval[1] - time_interval[0]))
    print "N time samples: %d" % (back["T"],)

    #if not cross_subject:

    valid_size = int(count_train * (valid_perc / 100.0))   
    X_valid, _ = random_subdictionary(X_train, valid_size)
    y_valid = {}

    for key in X_valid.keys():
        y_valid[key] = y_train.pop(key)

        subject_valid = getKeyFromValue(indices_train, key)

        indices_valid.setdefault(subject_valid, []).append(key)
        indices_train[subject_valid].remove(key)

    # Remapping

    for i, key in enumerate(sorted(X_train.keys())):
        X_train[i] = X_train.pop(key)
        y_train[i] = y_train.pop(key)

        subject_train = getKeyFromValue(indices_train, key)
        indices_train[subject_train].remove(key)
        indices_train[subject_train].append(i)

    
    for i, key in enumerate(sorted(X_valid.keys())):
        X_valid[i] = X_valid.pop(key)
        y_valid[i] = y_valid.pop(key)

        subject_valid = getKeyFromValue(indices_valid, key)
        indices_valid[subject_valid].remove(key)
        indices_valid[subject_valid].append(i)

    #else:
    #
    #    valid_size = count_valid-1;

    if save:
        savePath = root_dir
    else:
        savePath = None

    X_train, X_test, X_valid = preprocessingSets(
        X_train,
        X_test,
        X_valid,
        preprocessing,
        new_rate,
        sample_rate,
        savePath=savePath,
    )

    back["C"] = X_train[0].shape[0]
    back["T"] = X_train[0].shape[1]
    if new_rate:
        back["fs"] = new_rate

    back["train"] = BCIinMem(
        X_train, y_train, sample_rate, sample_transform, label_transform, name="BCI_IV", subjects= indices_valid
    )
    back["valid"] = BCIinMem(
        X_valid, y_valid, sample_rate, sample_transform, label_transform, name="BCI_IV", subjects= indices_valid
    )
    back["test"] = BCIinMem(
        X_test, y_test, sample_rate, sample_transform, label_transform, name="BCI_IV", subjects= indices_test
    )

    return back


def getKeyFromValue(_dict, value):

    for key in _dict.keys():
        if value in _dict[key]:
            return key